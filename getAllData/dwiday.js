const { toExcel } = require("../helper/helper");
const { getTasks, getBroadcast } = require("../services/BroadCastController");
const { getContacts } = require("../services/ContactController");
const { checkMessages } = require("../services/MessageController");
const { getAllUsers } = require("../services/UserController");

const token =
  "46ae365fa134bf3e1f140bf146c32346c0266996be2bfb2929a0a0c1af642b2e64bf";
const sender = "01DWIDAY";

const fetchMessages = async () => {
  try {
    const filter = { owner: sender };
    const { data } = await checkMessages({ token, filter });
    const msgs = data.data.msgs;
    toExcel(`${sender}Messages`, msgs);
  } catch (error) {
    console.log(error);
  }
};

const fetchContacts = async () => {
  try {
    const filter = { owner: sender };
    const { data } = await getContacts({ token, filter });
    const contacts = data.data.contacts;
    toExcel(`${sender}Contacts`, contacts);
  } catch (error) {
    console.log(error);
  }
};

const fetchTasks = async () => {
  try {
    const filter = { sender };
    const { data } = await getTasks({ token, filter });
    const tasks = data.data.tasks;
    toExcel(`${sender}Tasks`, tasks);
    // console.log(data);
  } catch (error) {
    console.log(error);
  }
};

const fetchBroadcasts = async () => {
  try {
    const filter = { sender };
    const { data } = await getBroadcast({ token, filter });
    const broadcast = data.data.broadcasts;
    toExcel(`${sender}Broadcast`, broadcast);
  } catch (error) {
    console.log(error);
  }
};

const fetchUsers = async () => {
  try {
    const filter = { "payload.sender": sender };
    const { data } = await getAllUsers({ token, filter });
    const users = data.data.allUsers.allUsers;
    toExcel(`${sender}Users`, users);
  } catch (error) {
    console.log(error);
  }
};

// fetchBroadcasts();
// fetchContacts();
// fetchUsers();
// fetchTasks();
fetchMessages();
