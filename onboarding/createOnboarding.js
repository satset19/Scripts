const { nanoid } = require("nanoid/non-secure");
const { readExcel, toJson, waitFor } = require("../helper/helper");
const { upsertConfig, postDaisiUrl } = require("../services/ConfigController");
// const fetchConfig = async ({ user, key }) => {
//     try {
//       const { data: config } = await getConfig({
//         token: user.token,
//         key: `${user.cluster}:${user.sender}-${key}`,
//       });
//       return config;
//     } catch (error) {
//       console.log(error);
//     }
//   };

const createOnboardingTemplate = async () => {
  try {
    const d = require("./02SAT1data.json");
    const c = "SAT1";
    const s = "02SAT1";
    const t =
      "6ac9fd48222b312c7c18646437db292040df3dd0234a2ea6e0087e373df00f60";
    const p = "./onboarding02SAT1.xlsx";

    const dataExcel = readExcel(p);

    const result = d.map((el) => {
      delete el.assignedTo;
      el.isRoundRobin = true;
      return el;
    });

    // const processOnboardingData = async (el) => {
    //   try {
    //     const data = {
    //       id: `${nanoid(8)}-${nanoid(4)}-${nanoid(4)}-${nanoid(4)}-${nanoid(
    //         12
    //       )}`,
    //       title: el.Kode,
    //       pattern: el["Onboarding Message"],
    //       reply: {
    //         text: el["Onboarding Reply"],
    //       },
    //       active: true,
    //       assignedTo: "Admin02",
    //       origin: el.Origin,
    //       tags: el.Tag,
    //       keepTags: "PUSH",
    //       keepAssignedTo: false,
    //       phone: 628111282885,
    //       isRoundRobin: false,
    //       useVoucher: true,
    //       silent: true,
    //     };

    //     // Variables processing...

    //     const text = data.pattern;
    //     const encodedText = encodeURIComponent(text);
    //     let origUrl = `https://api.whatsapp.com/send?phone=${data.phone}&text=${encodedText}`;
    //     const responseLink = await postDaisiUrl({
    //       origUrl,
    //       token: t,
    //     });
    //     data.pattern = "^" + data.pattern;

    //     const newOnboardingTemplate = {
    //       ...data,
    //       link: `https://go.daisi.id/${responseLink?.data?.url?.shortUrl}`,
    //       linkId: responseLink?.data?.url?._id,
    //     };

    //     return newOnboardingTemplate;
    //   } catch (error) {
    //     console.log("Error processing individual data:", error);
    //     throw error; // Re-throw the error to be caught by the outer catch block
    //   }
    // };

    // const dataUpload = await Promise.all(dataExcel.map(processOnboardingData));

    // await toJson(s + "data.json", dataUpload);

    const responseUpsert = await upsertConfig({
      token: t,
      //   key: `${c}:${s}-ONBOARDINGS`,
      //   value: result,
    });

    if (!responseUpsert.ok) {
      console.log("error Upsert");
    } else {
      console.logs("done");
      console.log(responseUpsert.config?.value);
      toJson(s + "data.json", result);
    }
  } catch (error) {
    console.log("Error during onboarding template creation:");
    console.error(error);
    // Handle or log the error as needed
  }
};

// console.log(
//   `${nanoid(8)}-${nanoid(4)}-${nanoid(4)}-${nanoid(4)}-${nanoid(12)}`
// );
const run = async () => {
  await createOnboardingTemplate();
};
run();
