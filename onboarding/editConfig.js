const { waitFor, toJson } = require("../helper/helper");
const { getConfig, changeConfig } = require("../services/ConfigController");

const token =
  "46ae365fa134bf3e1f140bf146c32346c0266996be2bfb2929a0a0c1af642b2e64bf";

const fetchConfig = async (sender, token) => {
  try {
    const key = `DWIDAY:${sender}-ONBOARDINGS`;
    const { data } = await getConfig({ token, key });
    const configs = data.data.config;
    const payload = {
      key,
      value: configs.value.map((el) => {
        el.silent = true;
        return el;
      }),
    };
    // console.log(configs.value);
    const res = await changeConfig({ token, payload });
    console.log(res);
    return sender;
  } catch (error) {
    console.log(error);
  }
};

async function* asyncDataGenerator(senders, token) {
  for (const sender of senders) {
    yield await fetchConfig(sender, token);
    await waitFor(500);
  }
}

async function main() {
  try {
    for await (const data of asyncDataGenerator(senders, token)) {
      console.log(data);
    }
  } catch (error) {
    console.log(error);
  }
}

const senders = [
  "01DWIDAY",
  "02DWIDAY",
  "03DWIDAY",
  "04DWIDAY",
  "05DWIDAY",
  "06DWIDAY",
  "07DWIDAY",
  "08DWIDAY",
  "09DWIDAY",
  "10DWIDAY",
  "12DWIDAY",
  "13DWIDAY",
  "14DWIDAY",
  "15DWIDAY",
  "16DWIDAY",
  "17DWIDAY",
  "18DWIDAY",
  "19DWIDAY",
  "20DWIDAY",
  "21DWIDAY",
  "22DWIDAY",
  "23DWIDAY",
  "24DWIDAY",
  "25DWIDAY",
  "26DWIDAY",
  "27DWIDAY",
  "28DWIDAY",
  "29DWIDAY",
  "30DWIDAY",
  "31DWIDAY",
  "32DWIDAY",
  "33DWIDAY",
  "34DWIDAY",
  "35DWIDAY",
  "36DWIDAY",
  "37DWIDAY",
  "38DWIDAY",
  "39DWIDAY",
  "40DWIDAY",
  "41DWIDAY",
  "42DWIDAY",
  "43DWIDAY",
  "44DWIDAY",
  "45DWIDAY",
  "46DWIDAY",
  "47DWIDAY",
  "48DWIDAY",
  "49DWIDAY",
  "50DWIDAY",
  "51DWIDAY",
  "52DWIDAY",
  "53DWIDAY",
  // "54DWIDAY",
  "55DWIDAY",
  // "56DWIDAY",
  // "57DWIDAY",
  // "58DWIDAY",
];

main();
