const { upsertConfig } = require("../services/ConfigController");
const { getAllUsers } = require("../services/UserController");

const senders = [
  "02PJTB",
  "03PJTB",
  "04PJTB",
  "05PJTB",
  "06PJTB",
  "07PJTB",
  "08PJTB",
  "09PJTB",
  "10PJTB",
  "11PJTB",
  "12PJTB",
  "13PJTB",
  "14PJTB",
  "15PJTB",
  "16PJTB",
  "17PJTB",
  "18PJTB",
  "19PJTB",
  "20PJTB",
  "21PJTB",
  "22PJTB",
  "23PJTB",
  "24PJTB",
  "25PJTB",
  "26PJTB",
  "27PJTB",
  "28PJTB",
  "29PJTB",
  "30PJTB",
  "31PJTB",
  "32PJTB",
  "33PJTB",
  "34PJTB",
  "35PJTB",
  "36PJTB",
  "37PJTB",
  "38PJTB",
  "39PJTB",
  "40PJTB",
  "41PJTB",
  "42PJTB",
  "43PJTB",
  "44PJTB",
  "45PJTB",
  "46PJTB",
  "47PJTB",
  "48PJTB",
  "49PJTB",
  "50PJTB",
  "51PJTB",
  "52PJTB",
  "53PJTB",
  "54PJTB",
  "55PJTB",
  "56PJTB",
  "57PJTB",
  "58PJTB",
  "59PJTB",
  "60PJTB",
  "61PJTB",
  "62PJTB",
  "63PJTB",
  "64PJTB",
  "65PJTB",
  "66PJTB",
  "67PJTB",
  "68PJTB",
  "69PJTB",
  "70PJTB",
  "71PJTB",
  "72PJTB",
  "73PJTB",
  "74PJTB",
  "85PJTB",
  "86PJTB",
];

const addContactMigration = async (sender) => {
  try {
    const usr = await getAllUsers({ filter: { "payload.sender": sender } });
    const user = usr.data.data.allUsers.allUsers[0];
    const cluster = user.payload.cluster;
    const key = `${cluster}:${sender}-CONTACT_MIGRATION`;
    const value = {
      tags: "CUSTOMER_NEW",
      origin: user.name,
      assignedTo: user.name,
      active: true,
    };
    const { data } = await upsertConfig({
      token: user.payload.token,
      key,
      value,
    });
    if (data.ok) {
      return `${user.name} = ${data.ok}`;
    }
    return `${user.name} = ${false}`;
  } catch (error) {
    console.log(error);
  }
};

async function* asyncDataGenerator(senders) {
  for (const sender of senders) {
    yield await addContactMigration(sender);
    // await waitFor(1000);
  }
}

async function main(senders) {
  try {
    for await (const data of asyncDataGenerator(senders)) {
      console.log(data);
    }
  } catch (error) {
    console.log(error);
  }
}

main(senders);
