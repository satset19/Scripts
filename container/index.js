const { default: axios } = require("axios");
const { waitFor, toJson } = require("../helper/helper");
const { getConfig, changeConfig } = require("../services/ConfigController");

async function fetchData(sender, token, cluster) {
  const key = `${cluster}:${sender}-INFO`;
  const { data } = await getConfig({ key, token });
  if (data.data.config) {
    // console.log(data.data);
    return data.data.config;
  } else {
    return { sender, config: data.data.config };
  }
}

async function* asyncDataGenerator(senders, token, cluster) {
  for (const payload of senders) {
    yield await fetchData(payload, token, cluster);
    await waitFor(1000);
  }
}

async function main() {
  const cluster = "PJTB";
  //   const sender = ["01SERVER", "02SERVER", "03SERVER", "04SERVER"];
  const sender = [
    "02PJTB",
    "04PJTB",
    "05PJTB",
    "06PJTB",
    "08PJTB",
    "09PJTB",
    "10PJTB",
    "11PJTB",
    "12PJTB",
    "13PJTB",
    "14PJTB",
    "15PJTB",
    "17PJTB",
    "18PJTB",
    "20PJTB",
    "21PJTB",
    "22PJTB",
    "23PJTB",
    "24PJTB",
    "25PJTB",
    "27PJTB",
    "28PJTB",
    "29PJTB",
    "30PJTB",
    "31PJTB",
    "32PJTB",
    "33PJTB",
    "34PJTB",
    "35PJTB",
    "36PJTB",
    "37PJTB",
    "38PJTB",
    "39PJTB",
    "40PJTB",
    "41PJTB",
    "43PJTB",
    "44PJTB",
    "45PJTB",
    "46PJTB",
    "47PJTB",
    "48PJTB",
    "49PJTB",
    "52PJTB",
    "53PJTB",
    "54PJTB",
    "55PJTB",
    "56PJTB",
    "57PJTB",
    "58PJTB",
    "60PJTB",
    "61PJTB",
    "63PJTB",
    "64PJTB",
    "65PJTB",
    "66PJTB",
    "67PJTB",
    "68PJTB",
    "69PJTB",
    "70PJTB",
    "71PJTB",
    "72PJTB",
    "73PJTB",
    "74PJTB",
  ];
  const token =
    "f36ab58b9176e27d163c4edd571b13ac1b1777c66c7a52d8859c6551156909b2";
  const tmp = [];
  const backup = [];
  for await (const data of asyncDataGenerator(sender, token, cluster)) {
    // console.log(data);
    tmp.push(data);

    //!!! KHUSUS BACKUP Di uncomment ketika pertama kali jalan script
    // backup.push(data);

    const numberOfSender = +data.key.match(/\d/g).join("");
    console.log(numberOfSender);
    // switch (true) {
    //   case numberOfSender === 1:
    //     break;
    //   case numberOfSender === 74:
    //     break;
    //   case numberOfSender <= 12:
    //     try {
    //       const name = `[HOT]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 15:
    //     try {
    //       const name = `[BDO]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 18:
    //     try {
    //       const name = `[BGR]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 22:
    //     try {
    //       const name = `[EMP]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 25:
    //     try {
    //       const name = `[KGD]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 34:
    //     try {
    //       const name = `[PAT]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 37:
    //     try {
    //       const name = `[PCP]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 41:
    //     try {
    //       const name = `[PIM]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 46:
    //     try {
    //       const name = `[PLI]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 49:
    //     try {
    //       const name = `[PLS]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 56:
    //     try {
    //       const name = `[PUR]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 61:
    //     try {
    //       const name = `[SMB]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 64:
    //     try {
    //       const name = `[SMS]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 66:
    //     try {
    //       const name = `[SUB]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 71:
    //     try {
    //       const name = `[PRI]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    //   case numberOfSender <= 73:
    //     try {
    //       const name = `[MOS]${data.value.name}`;
    //       const res = await changeConfig({
    //         token,
    //         payload: {
    //           ...data,
    //           value: {
    //             ...data.value,
    //             name,
    //           },
    //         },
    //       });
    //       if (res.data.ok) {
    //         tmp.push({
    //           key: res.data.config.key,
    //           before: data.value.name,
    //           after: name,
    //           status: res.data.ok,
    //         });
    //         console.log(`${res.data.config.key}: ${res.data.ok}`);
    //       }
    //       console.log(name);
    //     } catch (error) {
    //       console.log(error);
    //     }
    //     break;
    // }
  }
  await toJson(`${cluster}log.json`, tmp);
  //   await toJson(`${cluster}res.json`, tmp);

  //!!! KHUSUS BACKUP Di uncomment ketika pertama kali jalan script
  //   await toJson(`${cluster}Backup.json`, backup);
}

main();
