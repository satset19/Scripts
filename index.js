const xlsx = require("xlsx");
const jsonfile = require("jsonfile");
const {
  addContact,
  getContacts,
  deleteContact,
  updateContact,
} = require("./services/ContactController");
// const contacts = require("./contactDwiday.xlsx");

const p = "./Panorama_ContactUpdate_290424_rev.xlsx";

//Helper

const {
  filterMultipleTags,
  splitArrayToArray,
  splitBySender,
  waitFor,
  toJson,
  readExcel,
  countMultipleContact,
} = require("./helper/helper.js");
const { getAllUsers, updateUser } = require("./services/UserController");
const { addTag, getTags } = require("./services/TagController");

//CRUD
const addBulkContact = async () => {
  // const sender = "45DWIDAY";

  try {
    // Load the XLSX file
    const workbook = xlsx.readFile(p);

    // Specify the sheet you want to convert to JSON
    const sheetName = workbook.SheetNames[0]; // Change this to the name of your sheet

    // Convert the sheet to JSON
    const sheetData = xlsx.utils.sheet_to_json(workbook.Sheets[sheetName]);
    const arr = splitBySender(sheetData, "Owner");
    // console.log(arr);
    // Show data in JSON
    toJson("pp", arr);
    // toJson("ppp", data);
    // const tmpSucces = [];

    // const payload = {
    //   owner: sender,
    //   keepAssignedTo: false,
    //   keepTags: "REPLACE",
    //   keepOrigin: false,
    //   items: sheetData.map((el) => {
    //     return {
    //       name: el.PushName,
    //       phone: `${el.Phone}`,
    //       tags: `${el.Tags}`,
    //       counter: 0,
    //       assignedTo: "Admin",
    //       gender: "MALE",
    //       origin: "Direct Chat",
    //       pushName: `${el.PushName}`,
    //       types: "PERSONAL",
    //       agents: ["WA"],
    //     };
    //   }),
    // };
    // // console.log(payload);
    // const a = await addContact(token, payload);
    // console.log(a);
    // // await waitFor(500);
    // // toJson(`${sender}result`, a?.data);

    for (const sender in arr) {
      const usr = await getAllUsers({ filter: { "payload.sender": sender } });
      const user = usr.data.data.allUsers.allUsers[0];
      const token = user.payload.token;
      const contactsSplit = splitArrayToArray(arr[sender]);
      // const { data } = await getContacts({
      //   filter: { owner: sender },
      //   token,
      // });

      // toJson(`${sender}result`, data);
      // console.log(data);
      // await waitFor(500);
      // console.log(data.data.contacts);

      // Remove Duplicate
      // const removeDuplicate = arr[sender].filter(
      //   (el, idx) => arr[sender].map((e) => e.Phone).indexOf(el.Phone) === idx
      // );

      // const savedContacts = data.data.contacts.map((el) => {
      //   const tmp = arr[sender].find((ele) => ele.Phone == el.phone);
      //   el.tags = tmp?.Tag ? tmp?.Tag : el.tags;
      //   // el.name = tmp?.PushName ? tmp?.PushName : el.phoneNumber;
      //   el.origin = tmp?.Origin ? tmp?.Origin : el.origin;
      //   el.assignedTo = tmp?.AssignedTo ? tmp?.AssignedTo : el.assignedTo;

      //   return el;
      // });

      // toJson(`${sender}.json`, savedContacts);
      // console.log(`${sender} dataExcel: `, arr[sender].length);
      // console.log(`${sender} Contact: `, savedContacts.length);

      if (sender !== "10DWIDAY") {
        for (const batch in contactsSplit) {
          const payload = {
            owner: sender,
            keepAssignedTo: false,
            keepTags: "REPLACE",
            keepOrigin: true,
            items: contactsSplit[batch].map((el) => {
              return {
                // name: el["Customer Name"],
                phone: `${el.Phone}`,
                // tags: `${el["Tag Baru"]}`,
                // counter: 0,
                assignedTo: el.AssignedTo,
                // // gender: el.gender,
                // origin: el.Origin,
                // pushName: `${el["Customer Name"]}`,
                // types: "PERSONAL",
                // agents: ["WA"],
              };
            }),
          };

          const a = await addContact(token, payload);
          // await waitFor(1000);
          toJson(`${sender}${batch}`, a.data);
        }
      }

      // const payload = {
      //   owner: sender,
      //   keepAssignedTo: false,
      //   keepTags: "REPLACE",result
      //   keepOrigin: true,
      //   items: arr[sender].map((el) => {
      //     return {
      //       // name: el["Customer Name"],
      //       phone: `${el.Phone}`,
      //       // tags: `${el["Tag Baru"]}`,
      //       // counter: 0,
      //       assignedTo: el.AssignedTo,
      //       // // gender: el.gender,
      //       // origin: el.Origin,
      //       // pushName: `${el["Customer Name"]}`,
      //       // types: "PERSONAL",
      //       // agents: ["WA"],
      //     };
      //   }),
      // };
      // console.log(payload);
      // console.log(payload);
      // const a = await addContact(token, payload);
      // await waitFor(1000);
      // toJson(`${sender}result`, a.data);

      // toJson(`${sender}tmp`, payload);
      // console.log(a);

      // toJson(`${sender}BackUp.json`, data.data.contacts);
      // if (a) {
      //   tmpSucces.push(data[key]["Full Name"]);
      // }
      // console.log(`${key} contact uploaded`);
    }
  } catch (error) {
    console.log(error);
  }
};

const contactToExcel = async (token, sender) => {
  try {
    const phone = "6281293454217";
    const { data } = await getContacts({
      filter: { phone },
      token,
    });
    const contacts = data.data.contacts;

    const ws = xlsx.utils.json_to_sheet(contacts);
    const wb = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
    xlsx.writeFile(wb, `${sender}.xlsx`);

    // console.log(contacts);
  } catch (error) {
    console.log(error);
  }
};

const editContact = async (sender, token) => {
  // return sender;
  try {
    const { data } = await getContacts({
      filter: { owner: sender },
      token,
    });

    const havePushName = data.data.contacts.filter(
      (el) => el.pushName !== undefined && el.pushName !== null
      // el.group === "Emporium Pluit"
    );

    const emptyPushName = data.data.contacts.filter(
      (el) => el.pushName === null || !el.pushName || el.pushName === ""
    );

    const assigenToUndefined = data.data.contacts.filter(
      (el) => el.assignedTo === null || !el.assignedTo
    );

    // const arrContact = data.data.contacts;
    // console.log(arrContact);

    // if (havePushName.length > 0) {
    //   const mappedContact = splitArrayToArray(havePushName);

    //   const tmpSucces = [];
    //   for (const key in mappedContact) {
    //     const payload = {
    //       owner: sender,
    //       keepAssignedTo: false,
    //       keepTags: "PUSH",
    //       keepOrigin: true,
    //       items: mappedContact[key].map((el) => {
    //         return {
    //           phone: el.phone,
    //           assignedTo: assignedTo,
    //           pushName: el.pushName,
    //         };
    //       }),
    //     };
    //     const a = await addContact(token, payload);
    //     toJson(`${sender} ${key}`, havePushName);
    //   }
    // }
    if (emptyPushName.length > 0) {
      const mappedContact = splitArrayToArray(emptyPushName);
      for (const key in mappedContact) {
        const payload = {
          owner: sender,
          keepAssignedTo: false,
          keepTags: "REPLACE",
          keepOrigin: true,
          items: mappedContact[key].map((el) => {
            return {
              phone: el.phone,
              // assignedTo: assignedTo,
              pushName: el.phone,
            };
          }),
        };
        // console.log(payload);
        await addContact(token, payload);
        return `${sender}: ${emptyPushName.length} success`;
      }
    } else {
      return `${sender}: ${emptyPushName.length}`;
    }

    // toJson(s, assigenToUndefined);
    // console.log(`${sender}: ${emptyPushName.length}`);
  } catch (error) {
    console.log(error);
  }
};

const editContactByExcel = async (token) => {
  // return sender;
  try {
    const patch = "./Panorama_ContactUpdate_290424_rev.xlsx";
    const cx = readExcel(patch);
    const contacts = splitBySender(cx, "Owner");
    toJson("contt", contacts);
    // console.log(contacts);
    for (const sender in contacts) {
      if (sender !== "28PJTB" && sender !== "52PJTB") {
        const usr = await getAllUsers({ filter: { "payload.sender": sender } });
        const user = usr.data.data.allUsers.allUsers[0];
        const token = user.payload.token;
        // console.log(user);
        const payload = {
          owner: sender,
          keepAssignedTo: false,
          keepTags: "REPLACE",
          keepOrigin: true,
          items: contacts[sender].map((el) => {
            return {
              phone: `${el.Phone}`,
              // assignedTo: assignedTo,
              tags: `${el["Tag New"]}`,
            };
          }),
        };
        const { data } = await addContact(token, payload);
        console.log(data);
        console.log(sender);
        // console.log(payload);
      } else {
        if (sender === "28PJTB") {
          const token =
            "f36ab58b9176e27d163c4edd571b13ac1b1777c66c7a52d8859c6551156909b2";
          const payload = {
            owner: sender,
            keepAssignedTo: false,
            keepTags: "REPLACE",
            keepOrigin: true,
            items: contacts[sender].map((el) => {
              return {
                phone: `${el.Phone}`,
                // assignedTo: assignedTo,
                tags: `${el["Tag New"]}`,
              };
            }),
          };
          const { data } = await addContact(token, payload);
          console.log(data);
          console.log(sender);
          // console.log(payload);
        }
        if (sender === "52PJTB") {
          const token =
            "f36ab58b9176e27d163c4edd571b13ac1b1777c66c7a52d8859c6551156909b2";
          const payload = {
            owner: sender,
            keepAssignedTo: false,
            keepTags: "REPLACE",
            keepOrigin: true,
            items: contacts[sender].map((el) => {
              return {
                phone: `${el.Phone}`,
                // assignedTo: assignedTo,
                tags: `${el["Tag New"]}`,
              };
            }),
          };
          const { data } = await addContact(token, payload);
          console.log(data);
          console.log(sender);
          // console.log(payload);
        }
      }
    }
  } catch (error) {
    console.log(error);
  }
};

const deleteContactById = async (token, contactId) => {
  try {
    // Call the deleteContact function
    const result = await deleteContact(token, contactId);
    if (result) {
      console.log("Contact deleted successfully.");
    } else {
      console.log("Contact deletion failed.");
    }
  } catch (error) {
    console.error("Error deleting contact:", error);
  }
};

const bulkDeleteContact = async (token, sender) => {
  const tmpError = [];
  try {
    // const dataDelete = readExcel(p);
    const { data } = await getContacts({
      filter: { owner: sender },
      token,
    });
    const contacts = data.data.contacts;
    // const c = filterMultipleTags(contacts);
    const tmpSucces = [];

    // const filteredData = contacts
    //   .filter((el) => {
    //     return dataDelete.map((el) => el.Phone).indexOf(el.phone) > -1;
    //   })
    //   .filter((el) => el.assignedTo === null);
    // const tmp = contacts
    //   .filter((el) => el.tags !== null)
    //   .filter(
    //     (el) => el.tags.includes("UnknownBatch") && !el.tags.includes(",")
    //   );
    // toJson(`${sender} Contact`, c);

    // console.log(contacts);

    //Delete from Tags
    // for (const key in c) {
    //   // console.log(c[key][0].agents);
    //   const deleting = c[key].map(async (e) => {
    //     try {
    //       const { ok } = await deleteContact(token, e.id);
    //       if (ok) {
    //         return ok;
    //       }
    //     } catch (error) {
    //       return error;
    //     }
    //   });
    //   const promis = await Promise.all(deleting);
    //   toJson(`${sender} log`, promis);
    // }

    for (const contact in contacts) {
      const c = contacts[contact];
      const ok = await deleteContact(token, c.id);
      if (ok) {
        console.log(ok);
      }
      await waitFor(50);
    }

    // Normal Delete
    // filteredData.forEach(async (e) => {
    //   try {
    //     const ok = await deleteContact(token, e.id);
    //     // console.clear();
    //     console.log(ok);
    //     if (ok) {
    //       tmpSucces.push(e);
    //       console.log(`${e.phone} Deleted`);
    //     }
    //   } catch (error) {
    //     tmpError.push(error);
    //     console.log(`${e.phone} Error`);
    //   }
    // });

    console.log(contacts.length);
    // for (const contact in contacts) {
    //   const c = contacts[contact];
    //   const ok = await deleteContact(token, c.id);
    //   if (ok) {
    //     console.log(ok);
    //   }
    //   await waitFor(50);
    // }
  } catch (error) {
    console.log(error);
  }
};

const changeAssignedTo = async (sender) => {
  try {
    const usr = await getAllUsers({ filter: { "payload.sender": sender } });
    const user = usr.data.data.allUsers.allUsers[0];

    const { data } = await getContacts({
      filter: { owner: sender },
      token: user.payload.token,
    });

    const contacts = data.data.contacts;

    //contacts backup
    toJson(`${sender}.json`, contacts);

    const CUSTOMER_NEW = data.data.contacts.filter(
      (el) => el.tags.includes("CUSTOMER_NEW") === true
    );

    // console.log(user);
    if (contacts.length > 0) {
      const payload = {
        owner: sender,
        keepAssignedTo: false,
        keepTags: "REPLACE",
        keepOrigin: true,
        items: contacts.map((el) => {
          return {
            phone: el.phone,
            // assignedTo: assignedTo,
            pushName: `${el.pushName}`,
            assignedTo: user.name,
          };
        }),
      };
      await addContact(user.payload.token, payload);
      return `${sender}: ${contacts.length} success`;
    }
    return `${sender}: ${contacts.length} error`;
  } catch (error) {
    console.log(error);
  }
};

//
const editUser = async (token, sender, role) => {
  try {
    const filter = { "payload.sender": sender };

    const { data: users } = await getAllUsers({ filter });
    const d = users.data.allUsers;

    const p = d.allUsers;

    const data = p.map(async (el) => {
      const id = el.id;
      const email = el.email;
      // el.payload.group = group;
      el.payload.email = email;
      el.payload.role = role;

      delete el.email;
      delete el.id;
      delete el.createdAt;
      delete el.email;
      const { data } = await updateUser(id, token, el);
      return { ...data, email };
    });

    const r = await Promise.all(data);
    toJson(sender, r);
  } catch (error) {
    console.log(error);
  }
};

const addContactCluster = async (token, path, sender, d) => {
  try {
    const sheetData = readExcel(p);
    const dataFromExcel = splitBySender(sheetData);

    const tmp = [];
    const { data } = await getContacts({
      filter: {
        owner: sender,
      },
      token,
    });

    const fromExcel = d;

    const filteredContact = data.data.contacts
      .filter((el) => {
        // console.log(el);
        return fromExcel.map((ele) => ele.Phone).indexOf(el.phone) > -1;

        return (
          dataFromExcel[key]
            .map((ele) => ele.MobNumber.slice(1))
            .indexOf(el.phone.slice(2)) > -1
        );
      })
      .filter((el) => el.assignedTo === "None");
    const payload = {
      owner: sender,
      keepAssignedTo: false,
      keepTags: "KEEP",
      keepOrigin: true,
      // items: []
      //Default
      items: filteredContact.map((el) => {
        return {
          name: el.name !== null ? `${el.name}` : `${el.pushName}`,
          tags: el.tags,
          phone: el.phone,
          counter: 0,
          assignedTo: "Duta Mall Banjarmasin",
          gender: "MALE",
          // origin: el.origin.split(":")[1],
          pushName: `${el.pushName}`,
          // types: el.types,
          // agents: el.agents,
        };
      }),
    };
    toJson("payload.json", payload);
    const { data: res } = await addContact(token, payload);
    // console.log(payload.items.length);
    // console.log(filteredContact.length);
    // toJson(`${key}res.json`, res);
    // toExcel(`${key}HasOnboard`, filteredContact);
    console.log(res);

    //Checking Phone Number Sudah ada atau belum
    // for (const key in dataFromExcel) {
    //   if (key === "51DWIDAY") {
    //     const { data } = await getContacts({
    //       filter: {
    //         owner: key,
    //       },
    //       token,
    //     });
    //     const contactSender = data.data.contacts;
    //     //Contact yg sama tidak diambil
    //     const filteredContact = dataFromExcel[key].filter((el) => {
    //       // console.log(el);
    //       return (
    //         contactSender
    //           .map((ele) => ele.tags)
    //           .indexOf(`yellowbar${el.BranchName.replace(/\s/g, "")}`) > -1
    //       );

    //       return (
    //         dataFromExcel[key]
    //           .map((ele) => ele.MobNumber.slice(1))
    //           .indexOf(el.phone.slice(2)) > -1
    //       );
    //     });
    //     // .filter(
    //     //   (el) =>
    //     //     el.tags !== "Dwidayatourmanggadua,yellowbarManggaDua" &&
    //     //     el.tags.split(",")[1] !== "Dwidayatourmanggadua" &&
    //     //     el.tags.split(",")[2] !== "Dwidayatourmanggadua"
    //     // );
    //     // console.log(filteredContact.length);
    //     // toJson(key, filteredContact);

    //     // const payload = {
    //     //   owner: key,
    //     //   keepAssignedTo: false,
    //     //   keepTags: "PUSH",
    //     //   keepOrigin: true,
    //     //   items: []
    //     //   //Default
    //     //   items: filteredContact.map((el) => {
    //     //     return {
    //     //       name: el.name,
    //     //       phone: el.phone,
    //     //       tags: "Dwidayatourmanggadua,yellowbarManggaDua",
    //     //       counter: 0,
    //     //       assignedTo: el.assignedTo,
    //     //       gender: el.gender,
    //     //       origin: el.origin.split(":")[1],
    //     //       pushName: el.pushName,
    //     //       types: el.types,
    //     //       agents: el.agents,
    //     //     };
    //     //   }),
    //     // };

    //     // console.log(payload);
    //     console.log(filteredContact.length);
    //     toJson(key, filteredContact);
    //     // const payload = {
    //     //   owner: key,
    //     //   keepAssignedTo: true,
    //     //   keepTags: "PUSH",
    //     //   keepOrigin: true,
    //     //   // items: []
    //     //   //Default
    //     //   items: filteredContact.map((el) => {
    //     //     return {
    //     //       name: el.Name,
    //     //       phone: `62${el.MobNumber.slice(1)}`,
    //     //       tags: `Dwidayatour${el.BranchName.replace(
    //     //         /\s/g,
    //     //         ""
    //     //       ).toLowerCase()},yellowbar${el.BranchName.replace(/\s/g, "")}`,
    //     //       counter: 0,
    //     //       assignedTo: el.BranchName,
    //     //       gender: "MALE",
    //     //       origin: "EXCEL",
    //     //       pushName: el.Name,
    //     //       // types: el.types,
    //     //       // agents: el.agents,
    //     //     };
    //     //   }),
    //     // };
    //     // const { data: res } = await addContact(token, payload);
    //     // console.log(res);
    //     // console.log(filteredContact.length);
    //     // toJson(`${key}res.json`, res);
    //     // toExcel(`${key}HasOnboard`, filteredContact);
    //     // console.log(res.length);
    //     waitFor(5000);
    //   }
    // }

    // Show data in JSON
  } catch (error) {
    console.log(error);
  }
};

const insertTag = async (token, path) => {
  try {
    const token =
      "1241fb7c2a96a983aaa0fb6632ded68c8c2cb355ad474dcdda668c0ff862e7bd";

    // const dataExcel = readExcel(path);
    const dataExcel = [
      "FORTRE",
      "DENTAL",
      "HOTEL",
      "MEDE08",
      "TRAKI5",
      "UNICE17",
      "UNIC17",
      "SINA08",
      "MEDE63",
      "LPSIND",
    ];
    const log = [];
    for (const element of dataExcel) {
      // console.log(element);
      const payload = {
        tag: element,
        description: "",
        notes: "",
      };

      // console.log(payload);
      const { data } = await addTag(token, payload);
      console.log(data);
      // if (data.ok) {
      //   log.push({ id: data.tag._id, tag: data.tag.tag });
      // }
    }
    // toJson("tagInsert.json", log);
  } catch (error) {
    console.log(error);
  }
};

const checkTag = async (tags) => {
  try {
    const { data } = await getTags({
      token: "1241fb7c2a96a983aaa0fb6632ded68c8c2cb355ad474dcdda668c0ff862e7bd",
    });
    const Tags = data.data.tags.map((el) => el.tag);

    const res = tags.filter((el) => {
      return Tags.indexOf(el) === -1;
    });

    console.log(res);
  } catch (error) {
    console.log(error);
  }
};

// Run Program

const tags = [
  "ZURASU",
  "UNIQLO",
  "TRAKIN",
  "COCADI",
  "UNICEF",
  "MEDE01",
  "PUSRII",
  "MITADI",
  "FORTRE",
  "ENDRES",
  "ROCHEI",
  "SINARA",
  "PUTSAM",
  "ADVANT",
  "DENTAL",
  "OTOJAK",
  "NTTDAT",
  "RECBEN",
  "BANDAN",
  "ANDEKA",
  "OTOS01",
  "UNICE7",
  "REASUR",
  "BANTPS",
  "SINTOU",
  "JURONG",
  "HOTEL",
  "ANDHIK",
  "PRIENE",
  "TRIMUL",
  "INDRIL",
  "COECL3",
  "BUMAMA",
  "INERCO",
  "KOHIND",
  "ISLDEV",
  "ERIAAA",
  "MEDCO",
  "BANQNB",
  "HINOFI",
  "MANDOM",
  "TOPATL",
  "CIMBSE",
  "PENJAM",
  "MERCIP",
  "INFIDE",
  "BURVER",
  "NETLEP",
  "PERSIS",
  "SNVIND",
  "MEDE08",
  "TRAKI5",
  "TRAKI1",
  "INDEXM",
  "DURMET",
  "IKAPHA",
  "IKMSUB",
  "ORIPOL",
  "RETAIL",
  "BRENNT",
  "ZURTOP",
  "SOSINT",
  "TSUBAK",
  "UNICE17",
  "MAP",
  "UNIC17",
  "MEDE04",
  "HIJNAS",
  "SINA08",
  "MEDE28",
  "LOTTEP",
  "TAISIN",
  "BSNMED",
  "PEVAMI",
  "GOLD01",
  "MEDE63",
  "LIONSU",
  "MEGAHA",
  "LPSIND",
  "INTFED",
  "WORRES",
  "BOSOWA",
  "SUNSTR",
  "BATPRO",
  "KONEKS",
  "CENMEG",
  "GRAADH",
  "JAKSET",
  "INTERNAL",
];

// checkTag(tags);
// insertTag();

// const s = ["SMAILG02"];
const t =
  "46ae365fa134bf3e1f140bf146c32346c0266996be2bfb2929a0a0c1af642b2e64bf";
const s = [
  "01DWIDAY",
  "02DWIDAY",
  "03DWIDAY",
  "04DWIDAY",
  "05DWIDAY",
  "06DWIDAY",
  "07DWIDAY",
  "08DWIDAY",
  "09DWIDAY",
  "12DWIDAY",
  "13DWIDAY",
  "14DWIDAY",
  "15DWIDAY",
  "16DWIDAY",
  "17DWIDAY",
  "18DWIDAY",
  "19DWIDAY",
  "20DWIDAY",
  "21DWIDAY",
  "22DWIDAY",
  "23DWIDAY",
  "24DWIDAY",
  "25DWIDAY",
  "26DWIDAY",
  "27DWIDAY",
  "28DWIDAY",
  "29DWIDAY",
  "30DWIDAY",
  "31DWIDAY",
  "32DWIDAY",
  "33DWIDAY",
  "34DWIDAY",
  "35DWIDAY",
  "36DWIDAY",
  "37DWIDAY",
  "38DWIDAY",
  "39DWIDAY",
  "40DWIDAY",
  "41DWIDAY",
  "42DWIDAY",
  "43DWIDAY",
  "44DWIDAY",
  "45DWIDAY",
  "46DWIDAY",
  "47DWIDAY",
  "48DWIDAY",
  "49DWIDAY",
  "50DWIDAY",
  "51DWIDAY",
  "52DWIDAY",
  "53DWIDAY",
  "56DWIDAY",
  "57DWIDAY",
  "58DWIDAY",
];
const assignedTo = "Balikpapan";
const role = "admin";
// const s = [
//   "02PJTB",
//   "03PJTB",
//   "04PJTB",
//   "05PJTB",
//   "06PJTB",
//   "07PJTB",
//   "08PJTB",
//   "09PJTB",
//   "10PJTB",
//   "11PJTB",
//   "12PJTB",
//   "13PJTB",
//   "14PJTB",
//   "15PJTB",
//   "16PJTB",
//   "17PJTB",
//   "18PJTB",
//   "19PJTB",
//   "20PJTB",
//   "21PJTB",
//   "22PJTB",
//   "23PJTB",
//   "24PJTB",
//   "25PJTB",
//   "26PJTB",
//   "27PJTB",
//   "28PJTB",
//   "29PJTB",
//   "30PJTB",
//   "31PJTB",
//   "32PJTB",
//   "33PJTB",
//   "34PJTB",
//   "35PJTB",
//   "36PJTB",
//   "37PJTB",
//   "38PJTB",
//   "39PJTB",
//   "40PJTB",
//   "41PJTB",
//   "42PJTB",
//   "43PJTB",
//   "44PJTB",
//   "45PJTB",
//   "46PJTB",
//   "47PJTB",
//   "48PJTB",
//   "49PJTB",
//   "50PJTB",
//   "51PJTB",
//   "52PJTB",
//   "53PJTB",
//   "54PJTB",
//   "55PJTB",
//   "56PJTB",
//   "57PJTB",
//   "58PJTB",
//   "59PJTB",
//   "60PJTB",
//   "61PJTB",
//   "62PJTB",
//   "63PJTB",
//   "64PJTB",
//   "65PJTB",
//   "66PJTB",
//   "67PJTB",
//   "68PJTB",
//   "69PJTB",
//   "70PJTB",
//   "71PJTB",
//   "72PJTB",
//   "73PJTB",
//   "74PJTB",
//   "85PJTB",
//   "86PJTB",
// ];

// const s = ["01PJTB"];
async function* asyncDataGenerator() {
  for (const sender of s) {
    yield await editContact(sender, t);
    // yield await changeAssignedTo(sender);
    // await waitFor(1000);
  }
  // for (let i = 0; i < s.length; i++) {
  // }
}

async function main() {
  try {
    for await (const data of asyncDataGenerator()) {
      console.log(data);
    }
  } catch (error) {
    console.log(error);
  }
}

// main();
editContactByExcel();
// contactToExcel(t, s);
// addBulkContact();

// addContactCluster(t, p, s, data);
// insertTag(t, p);
// bulkDeleteContact(t, s);

// const s = "17DWIDAY";

// for (let i = 0; i < s.length; i++) {
// editContact(s[0], t);
//   // waitFor(10000);
//   // if (s[i] === "08DWIDAY" || s[i] === "15DWIDAY") {
//   // }
// }
// `Dwidayatour${el.BranchName.replace(
//   /\s/g,
//   ""
// ).toLowerCase()},yellowbar${el.BranchName.replace(/\s/g, "")}`;
