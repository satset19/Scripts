const xlsx = require("xlsx");
const jsonfile = require("jsonfile");

function splitArrayToArray(data) {
  let tmp = [];
  const result = {};
  let count = 0;
  data.forEach((e, idx) => {
    tmp.push(e);
    if (tmp.length === 1000 || idx + 1 === data.length) {
      count++;
      result[`batch${count}`] = tmp;
      tmp = [];
    }
    // if (e.tags.includes("UnknownBatch") && !e.tags.includes(",")) {
    //   tmp.push(e);
    // }
  });
  return result;
}

function filterMultipleTags(data) {
  let tmp = [];
  const result = {};
  let count = 0;
  data.forEach((e, idx) => {
    if (e.tags !== null) {
      if (
        e.tags.includes(",") &&
        e.tags.split(",")[0].includes("UnknownBatch") &&
        e.tags.split(",")[1].includes("UnknownBatch")
      ) {
        if (!result[e.tags]) result[e.tags] = [e];
        else result[e.tags].push(e);
      }
      if (e.tags.includes("UnknownBatch") && !e.tags.includes(",")) {
        if (!result[e.tags]) result[e.tags] = [e];
        else result[e.tags].push(e);
      }
    }
    // tmp.push(e);
  });
  return result;
}

function countMultipleContact(sheetData) {
  const filteredInvalidNumber = [];

  sheetData.forEach((item) => {
    const resObj = filteredInvalidNumber.find(
      (resObj) => resObj.Phone === item["No WhatsApp"]
    );
    resObj
      ? (resObj.Count++, resObj.MultipleNumber.push(item))
      : filteredInvalidNumber.push({
          Nama: item.Nama,
          Phone: item["No WhatsApp"],
          Count: 1,
          MultipleNumber: [],
        });
  });

  return filteredInvalidNumber.filter((el) => el.Count > 1);
  //   return filteredInvalidNumber;
}

function splitBySender(data, param) {
  const result = {};

  data.forEach((el, idx) => {
    if (idx === 0) {
      result[el[param]] = [el];
    } else {
      if (result[el[param]]) {
        result[el[param]].push(el);
      }
      if (!result[el[param]]) {
        result[el[param]] = [el];
      }
    }
    // for (const key in result) {
    //   if (key === el.Owner) {
    //     result[el.Owner].push(el);
    //   }
    // }
  });
  return result;
}

function splitBySender2(data) {
  const result = {};

  data.forEach((el, idx) => {
    if (idx === 0) {
      result[el.Owner] = [el];
    } else {
      if (result[el.Owner]) {
        result[el.Owner].push(el);
      }
      if (!result[el.Owner]) {
        result[el.Owner] = [el];
      }
    }
    // for (const key in result) {
    //   if (key === el.Owner) {
    //     result[el.Owner].push(el);
    //   }
    // }
  });
  return result;
}

const toJson = (sender, c) => {
  jsonfile.writeFile(sender, c, { spaces: 2 }, (err) => {
    if (err) {
      console.error("Error writing JSON file:", err);
    } else {
      console.log("Conversion complete. JSON data written to", `${sender}`);
    }
  });
};

const toExcel = (sender, contacts) => {
  const ws = xlsx.utils.json_to_sheet(contacts);
  const wb = xlsx.utils.book_new();
  xlsx.utils.book_append_sheet(wb, ws, "Sheet1");
  xlsx.writeFile(wb, `${sender}.xlsx`);
};

const waitFor = (time) => {
  return new Promise((resove) => setTimeout(resove, time));
};

const readExcel = (path, index = 0) => {
  // Load the XLSX file
  const workbook = xlsx.readFile(path);

  // Specify the sheet you want to convert to JSON
  const sheetName = workbook.SheetNames[index]; // Change this to the name of your sheet

  // Convert the sheet to JSON
  const sheetData = xlsx.utils.sheet_to_json(workbook.Sheets[sheetName]);
  return sheetData;
};

module.exports = {
  splitArrayToArray,
  countMultipleContact,
  filterMultipleTags,
  splitBySender,
  toExcel,
  toJson,
  waitFor,
  readExcel,
  splitBySender2,
};
