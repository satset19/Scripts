const { toExcel } = require("../helper/helper");
const { getMessageStats } = require("../services/MessageController");

//Data
const DWIDAY = {
  token: "46ae365fa134bf3e1f140bf146c32346c0266996be2bfb2929a0a0c1af642b2e64bf",
  sender: [
    "01DWIDAY",
    "02DWIDAY",
    // "03DWIDAY",
    // "04DWIDAY",
    // "05DWIDAY",
    // "06DWIDAY",
    // "07DWIDAY",
    // "08DWIDAY",
    // "09DWIDAY",
    // "12DWIDAY",
    // "13DWIDAY",
    // "14DWIDAY",
    // "15DWIDAY",
    // "16DWIDAY",
    // "17DWIDAY",
    // "18DWIDAY",
    // "19DWIDAY",
    // "20DWIDAY",
    // "21DWIDAY",
    // "22DWIDAY",
    // "23DWIDAY",
    // "24DWIDAY",
    // "25DWIDAY",
    // "26DWIDAY",
    // "27DWIDAY",
    // "28DWIDAY",
    // "29DWIDAY",
    // "30DWIDAY",
    // "31DWIDAY",
    // "32DWIDAY",
    // "33DWIDAY",
    // "34DWIDAY",
    // "35DWIDAY",
    // "36DWIDAY",
    // "37DWIDAY",
    // "38DWIDAY",
    // "39DWIDAY",
    // "40DWIDAY",
    // "41DWIDAY",
    // "42DWIDAY",
    // "43DWIDAY",
    // "44DWIDAY",
    // "45DWIDAY",
    // "46DWIDAY",
    // "47DWIDAY",
    // "48DWIDAY",
    // "49DWIDAY",
    // "50DWIDAY",
    // "51DWIDAY",
    // "52DWIDAY",
    // "53DWIDAY",
    // "56DWIDAY",
    // "57DWIDAY",
    // "58DWIDAY",
  ],
};

const PJTB = {
  token: "1241fb7c2a96a983aaa0fb6632ded68c8c2cb355ad474dcdda668c0ff862e7bd",
  sender: [
    "01PJTB",
    "02PJTB",
    // "03PJTB",
    // "04PJTB",
    // "05PJTB",
    // "06PJTB",
    // "07PJTB",
    // "08PJTB",
    // "09PJTB",
    // "10PJTB",
    // "11PJTB",
    // "12PJTB",
    // "13PJTB",
    // "14PJTB",
    // "15PJTB",
    // "16PJTB",
    // "17PJTB",
    // "18PJTB",
    // "19PJTB",
    // "20PJTB",
    // "21PJTB",
    // "22PJTB",
    // "23PJTB",
    // "24PJTB",
    // "25PJTB",
    // "26PJTB",
    // "27PJTB",
    // "28PJTB",
    // "29PJTB",
    // "30PJTB",
    // "31PJTB",
    // "32PJTB",
    // "33PJTB",
    // "34PJTB",
    // "35PJTB",
    // "36PJTB",
    // "37PJTB",
    // "38PJTB",
    // "39PJTB",
    // "40PJTB",
    // "41PJTB",
    // "42PJTB",
    // "43PJTB",
    // "44PJTB",
    // "45PJTB",
    // "46PJTB",
    // "47PJTB",
    // "48PJTB",
    // "49PJTB",
    // "50PJTB",
    // "51PJTB",
    // "52PJTB",
    // "53PJTB",
    // "54PJTB",
    // "55PJTB",
    // "56PJTB",
    // "57PJTB",
    // "58PJTB",
    // "59PJTB",
    // "60PJTB",
    // "61PJTB",
    // "62PJTB",
    // "63PJTB",
    // "64PJTB",
    // "65PJTB",
    // "66PJTB",
    // "67PJTB",
    // "68PJTB",
    // "69PJTB",
    // "70PJTB",
    // "71PJTB",
    // "72PJTB",
    // "73PJTB",
    // "74PJTB",
    // "75PJTB",
    // "76PJTB",
    // "77PJTB",
    // "78PJTB",
    // "79PJTB",
    // "80PJTB",
    // "81PJTB",
    // "82PJTB",
    // "85PJTB",
    // "86PJTB",
    // "87PJTB",
  ],
};

//Code
const fetchMsgStats = async ({ sender, token }) => {
  const payload = {
    clusters: sender.replace(/[^a-zA-Z]+/g, ""),
    sender: sender,
    //   "month": 0,
    year: 2024,
    //   "limit": 1,
    //   "skip": 0,
    groupBy: "MONTHLY",
  };
  //   return payload;"01PJTB"
  //   console.log(payload);
  const { data } = await getMessageStats({ token, payload });
  console.log(sender);
  return data.data;
};

async function* asyncGenerator(cluster) {
  const token = cluster.token;
  for (const sender of cluster.sender) {
    yield await fetchMsgStats({ token, sender });
  }
}

const main = async () => {
  let arr = [];

  //function test
  for await (const data of asyncGenerator(DWIDAY)) {
    const msgMapping = data.map((el) => {
      return {
        cluster: el.cluster,
        sender: el.owner,
        year: el.year,
        month: el.month,
        flow: el.flow,
        "total message": el.msgs,
      };
    });
    arr = arr.concat(msgMapping);
    console.log(arr);
  }

  //main function
  // for await (const data of asyncGenerator(DWIDAY)) {
  //   const msgMapping = data.map((el) => {
  //     return {
  //       cluster: el.cluster,
  //       sender: el.owner,
  //       year: el.year,
  //       month: el.month,
  //       flow: el.flow,
  //       "total message": el.msgs,
  //     };
  //   });
  //   arr = arr.concat(msgMapping);
  // }
  // for await (const data of asyncGenerator(PJTB)) {
  //   const msgMapping = data.map((el) => {
  //     return {
  //       cluster: el.cluster,
  //       sender: el.owner,
  //       year: el.year,
  //       month: el.month,
  //       flow: el.flow,
  //       "total message": el.msgs,
  //     };
  //   });
  //   arr = arr.concat(msgMapping);
  // }

  //export to excel
  //   toExcel("DWIDAY", arr);
};

main();
