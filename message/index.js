const { checkMessages } = require("../services/MessageController");
const { toJson, waitFor, toExcel } = require("../helper/helper");
const dayjs = require("dayjs");
// const mesasge = require("./query.json");
// console.log(mesasge.filter((el) => el.Flow !== "IN"));

const token =
  "1241fb7c2a96a983aaa0fb6632ded68c8c2cb355ad474dcdda668c0ff862e7bd";
const sender = "01PJTB";
const getMessage = async () => {
  // console.log(dayjs("04/24/2024").startOf("D").format());

  try {
    const { data } = await checkMessages({
      token,
      filter: {
        owner: sender,
        createdAt: {
          between: [
            `${dayjs("04/24/2024").startOf("D").format()}`,
            `${dayjs("04/25/2024").endOf("D").format()}`,
          ],
        },
        // cluster: "DWIDAY",
        // msg: { conversation: "UN" },
        // createdAt: {
        //   gt: dayjs("2024-01-26").startOf("D").format(),
        // },
      },
    });
    console.log(data);
    // const msgs = data.data.msgs
    //   .sort((a, b) => a.tStamp - b.tStamp)
    //   .map((el) => {
    //     const msg = el.msg;
    //     let Conversation = "";
    //     for (const key in msg) {
    //       switch (key) {
    //         case "extendedTextMessage":
    //           Conversation = msg.extendedTextMessage.text;
    //           break;
    //         case "documentMessage":
    //           Conversation = msg.documentMessage.fileName;
    //           break;
    //         case "imageMessage":
    //           Conversation =
    //             msg.imageMessage.caption !== null
    //               ? msg.imageMessage.caption
    //               : msg.imageMessage.mimetype;
    //           break;
    //         case "conversation":
    //           Conversation = msg.conversation;
    //           break;
    //         default:
    //           Conversation = msg.text;
    //           break;
    //       }
    //     }

    //     return {
    //       MessageID: el.id,
    //       "Customer Phone": el.flow === "IN" ? el.from : el.to,
    //       Company: "Panorama",
    //       TStamp: el.tStamp,
    //       Date: dayjs(el.tStamp * 1000).format("DD/MM/YYYY HH:mm:ss"),
    //       Conversation,
    //       Flow: el.flow,
    //     };
    //   });
    // toExcel(`${sender}`, msgs);
  } catch (error) {
    console.log(error);
  }
};

const filterMessage = async () => {
  try {
    const { data } = await checkMessages({
      token,
      filter: {
        owner: "85PJTB",
        // cluster: "DWIDAY",
        // msg: { conversation: "UN" },
        // createdAt: {
        //   gt: dayjs("2024-01-26").startOf("D").format(),
        // },
        flow: "OUT",
        "msg.imageMessage.caption": `✈ *Ini dia penawaran menarik dari Panorama JTB Special Cathay Pacific Travel Fair!*🛍✨\n\n*Dapatkan CASHBACK hingga Rp 8JUTA, bonus Travel Voucher up to Rp 5JUTA, ada juga bonus menarik lainnya : *Fujifilm Instax Mini & Apple Watch!* Nikmati *2x Panorama Points* untuk penawaran menarik liburan lainnya.\nAyooo, liburan keliling dunia ke *Korea, Jepang, Hong Kong dan China* dan kunjungi obyek wisata ikonik yang instagrammable. Segera hubungi *Panorama JTB!*\n\n#SiapLiburan\n#CathayPacificTravelFair`,
      },
    });
    const msgs = data.data.msgs.map((el) => {
      return {
        sender: el.owner,
        phoneNumber: el.to,
      };
    });
    toExcel(`${"cxtf"}`, msgs);
    console.log(msgs);
  } catch (error) {
    console.log(error);
  }
};

const filterMessage2 = async () => {
  try {
    const { data } = await checkMessages({
      token,
      filter: {
        owner: "85PJTB",
        // cluster: "PJTB",
        // msg: { conversation: "UN" },
        createdAt: {
          lte: dayjs("2024-02-28")
            .startOf("D")
            .format("YYYY-MM-DDTHH:mm:ssZ[Z]"),
        },
        flow: "OUT",
        // "msg.imageMessage.url": null,
      },
    });
    const msgs = data.data.msgs;
    toExcel(`${"cxtf"}`, msgs);
    console.log(msgs);
  } catch (error) {
    console.log(error);
  }
};

const reportMessageInOut = async () => {
  try {
    const msgsObj = {};
    mesasge.forEach((el) => {
      console.log(dayjs(el.TStamp * 1000).format("YYYY"));
    });
  } catch (error) {
    console.log(error);
  }
};
getMessage();
// filterMessage2();
