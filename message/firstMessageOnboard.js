const dayjs = require("dayjs");
const {
  readExcel,
  splitBySender,
  splitBySender2,
  toExcel,
} = require("../helper/helper");
const { checkMessages } = require("../services/MessageController");
const { getAllUsers } = require("../services/UserController");
const { getContacts } = require("../services/ContactController");
const p = "./dataSpike.xlsx";
const fExcel = readExcel(p, 0);
const dataSpike = splitBySender2(fExcel);

const fetchMessage = async (token, phoneNumber, owner) => {
  if (
    owner === "01DWIDAY" ||
    owner === "02DWIDAY" ||
    owner === "03DWIDAY" ||
    owner === "04DWIDAY" ||
    owner === "05DWIDAY" ||
    owner === "06DWIDAY" ||
    owner === "08DWIDAY" ||
    owner === "07DWIDAY"
  ) {
  }
  const { data } = await checkMessages({
    token,
    first: 20000,
    filter: { from: phoneNumber, owner },
  });
  const dataMsg = data.data.msgs.sort((a, b) => a.tStamp - b.tStamp)[0];

  const { data: dataContact } = await getContacts({
    token,
    filter: { phone: phoneNumber, owner },
  });
  const contact = dataContact.data.contacts[0];

  //////////////////////////////////////////////////////////////////////
  if (dataMsg) {
    if (
      dayjs(dataMsg.tStamp * 1000).format("DD/MM") ===
      dayjs(contact.createdAt).format("DD/MM")
    ) {
      for (const param in dataMsg.msg) {
        switch (param) {
          case "imageMessage":
            dataMsg.msg = dataMsg.msg[param].mimetype;
            break;
          case "extendedTextMessage":
            dataMsg.msg = dataMsg.msg[param].text;
            break;
          default:
            dataMsg.msg = dataMsg.msg[param];
        }
        dataMsg.msgType = param;
      }
      return dataMsg;
      // console.log(dataMsg);
    }

    return "wa contact";
  }
  // return "no chat";
  return "";
  //////////////////////////////////////////////////////////
  //Olah data
  //   if (dataMsg.length !== 0) {
  //     const res = dataMsg.map((el) => {
  //       el.dateTimeStamp = dayjs(el.tStamp * 1000).format("DD/MM/YYYY HH:mm");
  //       el.dateDaisi = dayjs(el.createdAt).format("DD/MM/YYYY HH:mm");
  //       for (const param in el.msg) {
  //         switch (param) {
  //           case "imageMessage":
  //             el.msg = el.msg[param].mimetype;
  //             break;
  //           case "extendedTextMessage":
  //             el.msg = el.msg[param].text;
  //             break;
  //           default:
  //             el.msg = el.msg[param];
  //         }
  //         el.msgType = param;
  //       }
  //       return el;
  //     });

  //     const updateRes = res.filter(
  //       (el) => el.dateTimeStamp.slice(0, 10) === el.dateDaisi.slice(0, 10)
  //     );
  //     if (updateRes.length !== 0) return updateRes[0];
  //     return {
  //       owner,
  //       tStamp: "-",
  //       flow: "-",
  //       msg: "no message",
  //       from: phoneNumber,
  //     };
  //     // Terus ke main function
  //     return res[0];
  //   }
  //   //terus ke main function jika gak ada msg
  //   return {
  //     owner,
  //     tStamp: "-",
  //     flow: "-",
  //     msg: "no message",
  //     from: phoneNumber,
  //   };
  //   return dataMsg;
};

async function* asyncDataGenerator() {
  for (const owner in dataSpike) {
    console.log(owner);
    //Get token from user container
    const { data } = await getAllUsers({ filter: { sender: owner } });
    const user = data.data.allUsers.allUsers[0];
    const token = data.data.allUsers.allUsers[0].payload.token;

    for (const pData of dataSpike[owner]) {
      yield await fetchMessage(token, pData.Phone, owner);
    }
  }
}

async function main() {
  const result = [];
  for await (const data of asyncDataGenerator()) {
    console.log(data);
    result.push(data);
  }
  await toExcel("ress", result);
}

main();
