const dayjs = require("dayjs");
const { toExcel, toJson, waitFor } = require("../helper/helper");
const { checkMessages } = require("../services/MessageController");
// const messages = require("./query.json");
const token =
  "46ae365fa134bf3e1f140bf146c32346c0266996be2bfb2929a0a0c1af642b2e64bf";
const sender = "01PJTB";
// const tstDummy = require("../Report.json");

const getMessage = async ({ skip, first }) => {
  try {
    const { data } = await checkMessages({
      first,
      skip,
      token,
      filter: {
        // owner: sender,
        cluster: "DWIDAY",
        // msg: { conversation: "UN" },
        // createdAt: {
        //   gt: dayjs("2024-01-26").startOf("D").format(),
        // },
      },
    });

    const msgs = data.data.msgs;

    console.log(data);
    return msgs;
  } catch (error) {
    console.log(error);
  }
};

const first = 100000;
const total = 5805253;
// getMessage({ skip: 0, first: 100 });
async function* asyncDataGenerator() {
  for (let i = 0; i <= total; i = i + first) {
    // const skip = i / first;
    // console.log(skip);
    yield await getMessage({ first, skip: i });
    // await waitFor(3000);
  }
}

async function main() {
  try {
    const reportData = {};
    let a = 1;
    let messages = [];
    for await (const data of asyncDataGenerator()) {
      console.log(data);
      console.log(a++);
      // await data.forEach((el) => {
      //   const date = `${dayjs(el.tStamp * 1000).format("MM/YYYY")}`;
      //   const YYYY = `${dayjs(el.tStamp * 1000).format("YYYY")}`;
      //   const MM = `${dayjs(el.tStamp * 1000).format("MMM")}`;
      //   // console.log(MM);
      //   if (!reportData[YYYY]) {
      //     reportData[YYYY] = {
      //       [MM]: {
      //         [el.owner]:
      //           el.flow === "IN" ? { IN: 1, OUT: 0 } : { IN: 0, OUT: 1 },
      //       },
      //     };
      //   } else {
      //     if (!reportData[YYYY][MM]) {
      //       reportData[YYYY][MM] = {
      //         [el.owner]:
      //           el.flow === "IN" ? { IN: 1, OUT: 0 } : { IN: 0, OUT: 1 },
      //       };
      //     } else {
      //       if (!reportData[YYYY][MM][el.owner]) {
      //         reportData[YYYY][MM][el.owner] =
      //           el.flow === "IN" ? { IN: 1, OUT: 0 } : { IN: 0, OUT: 1 };
      //       }
      //       el.flow === "IN"
      //         ? reportData[YYYY][MM][el.owner]["IN"]++
      //         : reportData[YYYY][MM][el.owner]["OUT"]++;
      //     }
      //   }
      //   // reportData[YYYY][MM]["total"] = reportData[YYYY][MM];
      // });
      // for (const year in reportData) {
      //   for (const month in reportData[year]) {
      //     for (const sender in reportData[year][month]) {
      //       messages.push({
      //         year,
      //         month,
      //         sender,
      //         cluster: "DWIDAY",
      //         IN: reportData[year][month][sender]["IN"],
      //         OUT: reportData[year][month][sender]["OUT"],
      //         total:
      //           reportData[year][month][sender]["IN"] +
      //           reportData[year][month][sender]["OUT"],
      //       });
      //     }
      //   }
      // }

      // toJson("DWIDAY.json", messages);
      // console.log(a++);
      // toJson("Report.json", reportData);
    }
  } catch (error) {
    console.log(error);
  }
}

main();
// console.log(tstDummy["2023"]["7"]["msgs"].length);
