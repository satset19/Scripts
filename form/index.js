const { waitFor, toExcel } = require("../helper/helper");
const { getForms } = require("../services/FormController");
const { getAllUsers } = require("../services/UserController");

const senders = [
  "01DWIDAY",
  "02DWIDAY",
  "03DWIDAY",
  "04DWIDAY",
  "05DWIDAY",
  "06DWIDAY",
  "07DWIDAY",
  "08DWIDAY",
  "09DWIDAY",
  "12DWIDAY",
  "13DWIDAY",
  "14DWIDAY",
  "15DWIDAY",
  "16DWIDAY",
  "17DWIDAY",
  "18DWIDAY",
  "19DWIDAY",
  "20DWIDAY",
  "21DWIDAY",
  "22DWIDAY",
  "23DWIDAY",
  "24DWIDAY",
  "25DWIDAY",
  "26DWIDAY",
  "27DWIDAY",
  "28DWIDAY",
  "29DWIDAY",
  "30DWIDAY",
  "31DWIDAY",
  "32DWIDAY",
  "33DWIDAY",
  "34DWIDAY",
  "35DWIDAY",
  "36DWIDAY",
  "37DWIDAY",
  "38DWIDAY",
  "39DWIDAY",
  "40DWIDAY",
  "41DWIDAY",
  "42DWIDAY",
  "43DWIDAY",
  "44DWIDAY",
  "45DWIDAY",
  "46DWIDAY",
  "47DWIDAY",
  "48DWIDAY",
  "49DWIDAY",
  "50DWIDAY",
  "51DWIDAY",
  "52DWIDAY",
  "53DWIDAY",
  "56DWIDAY",
  "57DWIDAY",
  "58DWIDAY",
];

const fetchForm = async (sender) => {
  try {
    const usr = await getAllUsers({ filter: { "payload.sender": sender } });
    const token = usr.data.data.allUsers.allUsers[0].payload.token;
    const user = usr.data.data.allUsers.allUsers[0];
    const { data } = await getForms({ sender, token });
    const forms = data.data;

    return forms;
  } catch (error) {
    console.log(error);
  }
};

async function* asyncDataGenerator() {
  for (const sender of senders) {
    yield await fetchForm(sender);
  }
}

async function main() {
  try {
    const result = [];
    for await (const data of asyncDataGenerator()) {
      data.forEach((el) => result.push(el));
    }
    toExcel(
      "DwidayaForm",
      result.map((el) => {
        el.responseIds = el.responseIds.length;
        delete el.sections;
        delete el.__v;

        return el;
      })
    );

    console.log(`${result.length} Done`);
  } catch (error) {
    console.log(error);
  }
}

main();
