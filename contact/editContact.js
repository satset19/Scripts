const { splitArrayToArray } = require("../helper/helper");
const { getContacts, addContact } = require("../services/ContactController");

const editContact = async (sender, token) => {
  // return sender;
  try {
    const { data } = await getContacts({
      filter: { owner: sender, phone: "62816858088" },
      token,
    });
    const contacts = data.data.contacts;

    const payload = {
      owner: sender,
      keepAssignedTo: false,
      keepTags: "REPLACE",
      keepOrigin: true,
      items: contacts.map((el) => {
        return {
          phone: el.phone,
          // assignedTo: assignedTo,
          //   pushName: el.phone,
          tags: "UNSUBSCRIBE",
        };
      }),
    };
    // console.log(payload);
    const { data: contactData } = await addContact(token, payload);
    console.log(contactData.contact);
    // return `${sender}: ${emptyPushName.length} success`;
    // const havePushName = data.data.contacts.filter(
    //   (el) => el.pushName !== undefined && el.pushName !== null
    //   // el.group === "Emporium Pluit"
    // );

    // const emptyPushName = data.data.contacts.filter(
    //   (el) => el.pushName === null || !el.pushName || el.pushName === ""
    // );

    // const assigenToUndefined = data.data.contacts.filter(
    //   (el) => el.assignedTo === null || !el.assignedTo
    // );

    // const arrContact = data.data.contacts;
    // console.log(arrContact);

    // if (havePushName.length > 0) {
    //   const mappedContact = splitArrayToArray(havePushName);

    //   const tmpSucces = [];
    //   for (const key in mappedContact) {
    //     const payload = {
    //       owner: sender,
    //       keepAssignedTo: false,
    //       keepTags: "PUSH",
    //       keepOrigin: true,
    //       items: mappedContact[key].map((el) => {
    //         return {
    //           phone: el.phone,
    //           assignedTo: assignedTo,
    //           pushName: el.pushName,
    //         };
    //       }),
    //     };
    //     const a = await addContact(token, payload);
    //     toJson(`${sender} ${key}`, havePushName);
    //   }
    // }

    // if (emptyPushName.length > 0) {
    //   const mappedContact = splitArrayToArray(emptyPushName);
    //   for (const key in mappedContact) {
    //     const payload = {
    //       owner: sender,
    //       keepAssignedTo: false,
    //       keepTags: "REPLACE",
    //       keepOrigin: true,
    //       items: mappedContact[key].map((el) => {
    //         return {
    //           phone: el.phone,
    //           // assignedTo: assignedTo,
    //           pushName: el.phone,
    //         };
    //       }),
    //     };
    //     // console.log(payload);
    //     await addContact(token, payload);
    //     return `${sender}: ${emptyPushName.length} success`;
    //   }
    // } else {
    //   return `${sender}: ${emptyPushName.length}`;
    // }

    // toJson(s, assigenToUndefined);
    // console.log(`${sender}: ${emptyPushName.length}`);
  } catch (error) {
    console.log(error);
  }
};

editContact(
  "47PJTB",
  "f36ab58b9176e27d163c4edd571b13ac1b1777c66c7a52d8859c6551156909b2"
);
