const { getContacts } = require("../services/ContactController");

const token =
  "1241fb7c2a96a983aaa0fb6632ded68c8c2cb355ad474dcdda668c0ff862e7bd";
const searchContact = async () => {
  try {
    const { data } = await getContacts({
      token,
      filter: { phone: 62816745106 },
    });
    console.log(
      data.data.contacts.map((el) => {
        return {
          sender: el.owner,
          tag: el.tags,
        };
      })
    );
  } catch (error) {
    console.log(error);
  }
};
searchContact();
