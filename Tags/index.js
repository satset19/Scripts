const { readExcel, waitFor } = require("../helper/helper");
const { addTag } = require("../services/TagController");

const token =
  "1241fb7c2a96a983aaa0fb6632ded68c8c2cb355ad474dcdda668c0ff862e7bd";
const sender = [];
const data = readExcel("./listTagPanorama.xlsx").map((el) => el.BUMAMA);
// const data = ["a", "b", "c"];

async function generator() {
  for (const key of data) {
    const payload = {
      tag: key,
      description: "",
      notes: "",
    };
    const { data } = await addTag(token, payload);
    if (data.ok) {
      console.log(data.ok);
    } else {
      console.log(key + " Error");
    }
    await waitFor(500);
  }
}
generator();
