const { default: axios } = require("axios");
require("dotenv").config();
// const ss = `   to
// from
// msg
// createdAt
//
// cluster
// flow
//           id
// updatedAt
// readAt`;
const checkMessages = async (
  { token, filter, skip, first, orderBy = "-createdAt" },
  options
) => {
  const payload = {
    query: `query msgs($skip: Int, $first: Int, $filter: JSON, $orderBy: String) { 
        msgs(skip: $skip, first: $first, filter: $filter, orderBy: $orderBy) {
          owner
          tStamp
          flow
          msg
          from
          createdAt
        }
      }`,
    variables: {
      first,
      skip,
      filter,
      orderBy,
    },
  };
  const url = `${process.env.API_DB_URL}/graphql`;

  return axios({
    method: "POST",
    url,
    headers: {
      "Content-Type": "application/json",
      token,
    },
    data: payload,
    skipErrorHandler: true,
    ...(options || {}),
  });
};

async function getResponseTimes(payload, options) {
  const payloadGQL = {
    query: `query responseTimes($skip: Int, $first: Int, $filter: JSON) {
      responseTimes(skip: $skip, first: $first, filter: $filter) {
        jid
        cluster
        sender
        msgId
        responseMsgId
        time
        timestamp
        from
      }
    }`,
    variables: {
      filter: payload.filter || {},
      first: 100000,
      skip: 0,
    },
  };

  return axios({
    method: "POST",
    url: `${process.env.API_DB_URL}/graphql`,
    headers: {
      "Content-Type": "application/json",
      token: payload.token,
      key: process.env.DB_DAISI_KEY,
    },
    data: payloadGQL,
    skipErrorHandler: true,
    ...(options || {}),
  });
}

async function getMessageStats({ payload, token }) {
  const url = `${process.env.API_DB_URL}/api/v1/msg/stats`;
  return axios({
    method: "POST",
    url,
    headers: {
      "Content-Type": "application/json",
      token,
    },
    data: payload,
  });
}

module.exports = {
  checkMessages,
  getResponseTimes,
  getMessageStats,
};
