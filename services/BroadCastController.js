const { default: axios } = require("axios");

require("dotenv").config();

const broadcastProcess = async (payload, token) => {
  try {
    const url = `${process.env.API_DB_DEV_URL}/api/v1/broadcast/get-progress`;
    const { sender, batchId } = payload;

    return axios({
      method: "post",
      url,
      headers: { "Content-Type": "application/json", token },
      data: {
        sender,
        batchId,
      },
    });
  } catch (error) {
    return error;
  }
};

const broadcastScheduled = async (payload, token) => {
  try {
    const url = `${process.env.API_DB_URL}/api/v1/agendash`;
    return axios({
      method: "post",
      url,
      headers: {
        "Content-Type": "application/json",
        token,
      },
      data: payload,
    });
  } catch (error) {
    console.log(error);
  }
};

const getTasks = async (
  { token, filter, skip, first, orderBy = "-createdAt" },
  options
) => {
  try {
    const url = `${process.env.API_DB_DEV_URL}/graphql`;
    const payload = {
      query: `query tasks($skip: Int, $first: Int, $filter: JSON, $orderBy: String) { 
        tasks(skip: $skip, first: $first, filter: $filter, orderBy: $orderBy) {
          id
          company
          batch
          n
          T
          msg
          sender
          phone
          label
          userId
          status 
        }
      }`,
      variables: {
        first: 200000,
        skip: skip * first,
        filter,
        orderBy,
      },
    };

    return axios({
      method: "POST",
      url,
      headers: {
        "Content-Type": "application/json",
        token,
      },
      data: payload,
      skipErrorHandler: true,
      ...(options || {}),
    });
  } catch (error) {
    console.log(error);
  }
};

const processingMsgQueue = async ({ payload, token }) => {
  try {
    let URL = `${process.env.API_AGENDA_URL}/api/v1/job`;
    // const payload = {
    //   data: {
    //     body: {
    //       ...value,
    //       excludes: "",
    //       sender: option.sender,
    //       userId: option.name,
    //     },
    //     headers: {
    //       "Content-Type": "application/json",
    //       token,
    //     },
    //   },
    //   interval: option.interval,
    //   name: "send-wa-v2",
    //   type: option.type,
    // };
    // option.type === "now" ? delete payload.interval : "";

    const data = await axios({
      method: "post",
      url: URL,
      headers: {
        "Content-Type": "application/json",
        token,
      },
      data: payload,
    });
    // console.log(data);
    // if (data?.message === 'Job run') return 'Broadcast is sending';
    // else if (data?.message === 'Job scheduled')
    //   return `Broadcast will be sent on ${inter}`;
    // else {
    //   return `Broadcast failed`;
    // }
    return data;
  } catch (err) {
    const msg = `${err.message}. ${err.response ? err.response.data : ""}`;
    console.log("queue msg error: %s", msg);
    return err;
    // toast.error(msg);
  }
};

async function getBroadcast({ token, filter, skip, first }) {
  const payload = {
    query: `query broadcasts($skip: Int, $first: Int, $filter: JSON, $orderBy: String) {
      broadcasts(skip: $skip, first: $first, filter: $filter, orderBy: $orderBy) {
        id
        name
        cluster
        sender
        broadcasterId
        batchId
        messages
        tags
        status
        totalTask
        createdAt
        endedAt
      }
    }`,
    variables: {
      first: 10000,
      skip: skip * first,
      filter,
    },
  };

  return axios({
    method: "POST",
    url: `${process.env.API_DB_DEV_URL}/graphql`,
    headers: {
      "Content-Type": "application/json",
      token,
    },
    data: payload,
  });
}

module.exports = {
  broadcastProcess,
  broadcastScheduled,
  getTasks,
  processingMsgQueue,
  getBroadcast,
};
