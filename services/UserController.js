const axios = require("axios");
require("dotenv").config();

async function getAllUsers(
  { filter = {}, first = 10000, skip = 0, orderBy = "-createdAt" },
  options
) {
  const payload = {
    query: `query allUsers($skip: Int, $first: Int, $filter: JSON, $orderBy: String) { 
        allUsers(skip: $skip, first: $first, filter: $filter, orderBy: $orderBy) {
          total
          allUsers {
            id,
            payload,
            email,
            name,
            role,
            createdAt
          }
        }
      }`,
    variables: {
      filter,
      first,
      skip,
      orderBy,
    },
  };

  return axios(`${process.env.API_DB_URL}/graphql`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      token: process.env.HBINDUNI_TOKEN,
      key: process.env.DB_DAISI_KEY,
    },
    data: payload,
    skipErrorHandler: true,
    ...(options || {}),
  });
}

async function updateUser(id, token, payload) {
  try {
    return axios({
      method: "patch",
      url: `${process.env.API_DB_DEV_URL}/api/v1/user/${id}`,
      headers: {
        token,
      },
      data: payload,
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports = {
  getAllUsers,
  updateUser,
};
