const axios = require("axios");
require("dotenv").config();

async function addTag(token, payload) {
  return axios({
    url: `${process.env.API_DB_URL}/api/v1/tag`,
    method: "post",
    headers: {
      token,
    },
    data: payload,
  });
}

async function getTags(payload, options) {
  const payloadGQL = {
    query: `query tags($skip: Int, $first: Int, $filter: JSON, $orderBy: String) { 
        tags(skip: $skip, first: $first, filter: $filter, orderBy: $orderBy) {  
          id
          tag
        }
      }`,
    variables: {
      filter: {
        company: payload.cluster,
      },
      first: 100000,
      skip: 0,
      orderBy: "-tag",
    },
  };

  return axios({
    method: "POST",
    url: `${process.env.API_DB_URL}/graphql`,
    headers: {
      "Content-Type": "application/json",
      token: payload.token,
    },
    data: payloadGQL,
    skipErrorHandler: true,
    ...(options || {}),
  });
}

module.exports = {
  addTag,
  getTags,
};
