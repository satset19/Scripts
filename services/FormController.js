const { default: axios } = require("axios");

async function getForms({ sender, token }, options) {
  return axios({
    method: "GET",
    url: `https://daisi-bot-servicec.et.r.appspot.com/api/v1/forms?sender=${sender}`,
    headers: {
      "Content-Type": "application/json",
      token,
    },
    skipErrorHandler: true,
    ...(options || {}),
  });
}

module.exports = {
  getForms,
};
