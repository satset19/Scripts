const axios = require("axios");
require("dotenv").config();

const upsertConfig = async ({ token, key, value }, options) => {
  const payload = {
    key,
    value,
  };

  return axios(`${process.env.API_DB_URL}/api/v1/config`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      token,
    },
    data: payload,
    skipErrorHandler: true,
    ...(options || {}),
  });
};

/** Create Daisi URL POST /api/v1/url */
const postDaisiUrl = async ({ origUrl, token }) => {
  const url = `${process.env.API_DB_URL}/api/v1/url`;
  const payload = {
    origUrl,
  };

  const options = {
    headers: {
      "Content-Type": "application/json",
      token,
    },
  };

  return axios(url, {
    method: "POST",
    data: payload,
    skipErrorHandler: true,
    ...(options || {}),
  });
};

/** Create Daisi URL POST /api/v1/url */
const patchDaisiUrl = async ({ origUrl, token, linkId }) => {
  const url = `${process.env.API_DB_URL}/api/v1/url/${linkId}`;
  const payload = {
    origUrl,
  };

  const options = {
    headers: {
      "Content-Type": "application/json",
      token,
    },
  };

  return axios(url, {
    method: "PATCH",
    data: payload,
    skipErrorHandler: true,
    ...(options || {}),
  });
};

async function getConfig({ token, key }, options) {
  const payloadGQL = {
    query: `query config($key: String) { 
      config(key: $key) {  
        key
        value
      }
    }`,
    variables: {
      key,
    },
  };

  return axios(`${process.env.API_DB_DEV_URL}/graphql`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      token,
    },
    data: payloadGQL,
    skipErrorHandler: true,
    ...(options || {}),
  });
}

async function changeConfig({ token, payload }) {
  const options = {
    headers: {
      "Content-Type": "application/json",
      token,
    },
  };
  return axios(`${process.env.API_DB_DEV_URL}/api/v1/config`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      token,
    },
    data: payload,
    skipErrorHandler: true,
    ...(options || {}),
  });
}

module.exports = {
  upsertConfig,
  postDaisiUrl,
  patchDaisiUrl,
  getConfig,
  changeConfig,
};
