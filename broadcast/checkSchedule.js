const dayjs = require("dayjs");
const { broadcastScheduled } = require("../services/BroadCastController");
const { toExcel } = require("../helper/helper");
require("dotenv").config();
const checkBroadcastScheduled = async (token, sender) => {
  try {
    const payload = {
      sender,
      limit: 10,
      skip: 0,
      isOpen: true,
    };
    const { data } = await broadcastScheduled(payload, token);
    const { ok, total } = data;
    // if (ok && total > 0) {
    //   // const time = data.data.map((el) => {
    //   //   console.log(el);
    //   //   if (el.nextRunAt !== null) {
    //   //     return {
    //   //       nextRunAt: `${el.nextRunAt}`,
    //   //       //   date: `${new Date(el.nextRunAt).get("id-ID").slice(0, 5)}/tot`,
    //   //     };
    //   //   }
    //   //   return {
    //   //     lastFinishedAt: `${dayjs(el.lastFinishedAt).format(
    //   //       "DD/MM/YYYY HH:mm"
    //   //     )}`,
    //   //     //   date: `${new Date(el.nextRunAt).get("id-ID").slice(0, 5)}/tot`,
    //   //   };
    //   // });
    //   // //   console.log(`${sender}: ${total} Scheduled at ${time}`);
    //   // const res = {
    //   //   sender,
    //   //   total,
    //   //   data: time,
    //   // };
    //   // console.log(res);
    // }
    console.log(data);
    const time = data.data.map((el) => {
      return {
        sender,
        time: `${dayjs(el.lastRunAt).format("DD/MM/YYYY HH:mm")}`,
        CreatedAt: el.lastRunAt,
        // data: el.data.body,
        BroadcasterId: el.data.body.userId,
        TotalTask: total,
        Tags: el.data.body.tags,
        phones: el.data.body.phones,
        // messages: el.data.body,
        Text: el.data.body.messages[0].text,
        url: el.data.body.messages[0].image?.url,
        caption: el.data.body.messages[0].caption,
      };
    });
    const filteredTime = time.filter((el) => el.time.slice(0, 5) === "30/04");

    if (filteredTime.length > 0) {
      // console.log(filteredTime);
      // toExcel(`${sender}`, filteredTime);
    }
    // console.log(data);
    // data.data.forEach((el) => {
    //   console.log(el.data);
    // });
  } catch (error) {
    console.log(error);
  }
};

// process.env.HBINDUNI_TOKEN;

const tDwiday =
  "46ae365fa134bf3e1f140bf146c32346c0266996be2bfb2929a0a0c1af642b2e64bf";
const DWIDAY = [
  "01DWIDAY",
  "02DWIDAY",
  "03DWIDAY",
  "04DWIDAY",
  "05DWIDAY",
  "06DWIDAY",
  "07DWIDAY",
  "08DWIDAY",
  "09DWIDAY",
  "10DWIDAY",
  "11DWIDAY",
  "12DWIDAY",
  "13DWIDAY",
  "14DWIDAY",
  "15DWIDAY",
  "16DWIDAY",
  "17DWIDAY",
  "18DWIDAY",
  "19DWIDAY",
  "20DWIDAY",
  "21DWIDAY",
  "22DWIDAY",
  "23DWIDAY",
  "24DWIDAY",
  "25DWIDAY",
  "26DWIDAY",
  "27DWIDAY",
  "28DWIDAY",
  "29DWIDAY",
  "30DWIDAY",
  "31DWIDAY",
  "32DWIDAY",
  "33DWIDAY",
  "34DWIDAY",
  "35DWIDAY",
  "36DWIDAY",
  "37DWIDAY",
  "38DWIDAY",
  "39DWIDAY",
  "40DWIDAY",
  "41DWIDAY",
  "42DWIDAY",
  "43DWIDAY",
  "44DWIDAY",
  "45DWIDAY",
  "46DWIDAY",
  "47DWIDAY",
  "48DWIDAY",
  "49DWIDAY",
  "50DWIDAY",
  "51DWIDAY",
  "52DWIDAY",
  "53DWIDAY",
  "54DWIDAY",
  "55DWIDAY",
  "56DWIDAY",
  "57DWIDAY",
  "58DWIDAY",
];

const t = "f36ab58b9176e27d163c4edd571b13ac1b1777c66c7a52d8859c6551156909b2";
const PJTB = [
  "02PJTB",
  "03PJTB",
  "04PJTB",
  "05PJTB",
  "06PJTB",
  "07PJTB",
  "08PJTB",
  "09PJTB",
  "10PJTB",
  "11PJTB",
  "12PJTB",
  "13PJTB",
  "14PJTB",
  "15PJTB",
  "16PJTB",
  "17PJTB",
  "18PJTB",
  "19PJTB",
  "20PJTB",
  "21PJTB",
  "22PJTB",
  "23PJTB",
  "24PJTB",
  "25PJTB",
  "26PJTB",
  "27PJTB",
  "28PJTB",
  "29PJTB",
  "30PJTB",
  "31PJTB",
  "32PJTB",
  "33PJTB",
  "34PJTB",
  "35PJTB",
  "36PJTB",
  "37PJTB",
  "38PJTB",
  "39PJTB",
  "40PJTB",
  "41PJTB",
  "42PJTB",
  "43PJTB",
  "44PJTB",
  "45PJTB",
  "46PJTB",
  "47PJTB",
  "48PJTB",
  "49PJTB",
  "50PJTB",
  "51PJTB",
  "52PJTB",
  "53PJTB",
  "54PJTB",
  "55PJTB",
  "56PJTB",
  "57PJTB",
  "58PJTB",
  "59PJTB",
  "60PJTB",
  "61PJTB",
  "62PJTB",
  "63PJTB",
  "64PJTB",
  "65PJTB",
  "66PJTB",
  "67PJTB",
  "68PJTB",
  "69PJTB",
  "70PJTB",
  "71PJTB",
  "72PJTB",
  "73PJTB",
  "74PJTB",
  "84PJTB",
  "85PJTB",
  "86PJTB",
  "87PJTB",
  "88PJTB",
];

PJTB.forEach((el) => {
  checkBroadcastScheduled(t, el);
});

// console.log(
//   new Date("2023-12-06T02:00:00.000Z").toLocaleTimeString("id-ID").slice(0, 5)
// );
