const { waitFor, readExcel } = require("../helper/helper.js");
const { processingMsgQueue } = require("../services/BroadCastController.js");
const { getAllUsers } = require("../services/UserController.js");
const path = "./PanoramaBroadcastPlan.xlsx";
const token =
  "d405a1c9c00571708a7ab8d877f3163b72f2f563c529d7d2b99361cc9e0bfc6f";

const tags = [
  "ZURASU",
  "UNIQLO",
  "TRAKIN",
  "COCADI",
  "UNICEF",
  "MEDE01",
  "PUSRII",
  "MITADI",
  "FORTRE",
  "ENDRES",
  "ROCHEI",
  "SINARA",
  "PUTSAM",
  "ADVANT",
  "DENTAL",
  "OTOJAK",
  "NTTDAT",
  "RECBEN",
  "BANDAN",
  "ANDEKA",
  "OTOS01",
  "UNICE7",
  "REASUR",
  "BANTPS",
  "SINTOU",
  "JURONG",
  "HOTEL",
  "ANDHIK",
  "PRIENE",
  "TRIMUL",
  "INDRIL",
  "COECL3",
  "BUMAMA",
  "INERCO",
  "KOHIND",
  "ISLDEV",
  "ERIAAA",
  "MEDCO",
  "BANQNB",
  "HINOFI",
  "MANDOM",
  "TOPATL",
  "CIMBSE",
  "PENJAM",
  "MERCIP",
  "INFIDE",
  "BURVER",
  "NETLEP",
  "PERSIS",
  "SNVIND",
  "MEDE08",
  "TRAKI5",
  "TRAKI1",
  "INDEXM",
  "DURMET",
  "IKAPHA",
  "IKMSUB",
  "ORIPOL",
  "RETAIL",
  "BRENNT",
  "ZURTOP",
  "SOSINT",
  "TSUBAK",
  "UNICE17",
  "MAP",
  "UNIC17",
  "MEDE04",
  "HIJNAS",
  "SINA08",
  "MEDE28",
  "LOTTEP",
  "TAISIN",
  "BSNMED",
  "PEVAMI",
  "GOLD01",
  "MEDE63",
  "LIONSU",
  "MEGAHA",
  "LPSIND",
  "INTFED",
  "WORRES",
  "BOSOWA",
  "SUNSTR",
  "BATPRO",
  "KONEKS",
  "CENMEG",
  "GRAADH",
  "JAKSET",
  "INTERNAL",
];

const createBroadcast = async (payload) => {
  try {
    const usr = await getAllUsers({ filter: { email: payload.Email } });
    const token = usr.data.data.allUsers.allUsers[0].payload.token;
    const user = usr.data.data.allUsers.allUsers[0];
    // console.log(user.name);
    const time = payload.Date.split("/");
    // console.log(time);
    const payloadBc = {
      data: {
        body: {
          label: payload.Label,
          messages: [
            {
              caption: payload.Materi,
              image: {
                url: payload.Image,
              },
            },
          ],
          tags: payload.Tag.split(", ").join(","),
          // tags: payload.Tag,
          // tags: tags.join(","),
          //   excludes: "",
          sender: payload.SenderID,
          userId: user.name,
        },
        headers: {
          "Content-Type": "application/json",
          token,
        },
      },
      interval: `${time[1]}/${time[0]}/${time[2].split(":")[0]}:${
        time[2].split(":")[1]
      }`,
      name: "send-wa-v2",
      type: "schedule",
    };
    // console.log(usr.data.datda.allUsers);
    // console.log(payload.Materi);

    const { data } = await processingMsgQueue({
      payload: payloadBc,
      token,
    });
    return data;
    return payloadBc;
  } catch (error) {
    console.log(error);
  }
};

async function* asyncDataGenerator(payloads, token) {
  for (const payload of payloads) {
    yield await createBroadcast(payload, token);
    await waitFor(1000);
  }
}

const payloads = readExcel(path, 3);
// console.log(payloads);

async function main() {
  try {
    let a = 0;
    for await (const data of asyncDataGenerator(payloads, token)) {
      a++;
      console.log(data);
    }
    console.log(a);
  } catch (error) {
    console.log(error);
  }
}

main();
