const createBroadcast = async (payload) => {
  try {
    //   const usr = await getAllUsers({ filter: { email: payload.Email } });
    //   const token = usr.data.data.allUsers.allUsers[0].payload.token;
    //   const user = usr.data.data.allUsers.allUsers[0];
    // console.log(user.name);
    // const time = payload.Time.split(":");
    // time.pop();
    const payloadBc = {
      name: "send-wa-v3",
      type: "schedule",
      interval: "Sat, 03 Feb 2024 08:00:00 GMT+7",
      data: {
        headers: {
          "Content-Type": "application/json",
          token:
            "1241fb7c2a96a983aaa0fb6632ded68c8c2cb355ad474dcdda668c0ff862e7bd",
        },
        body: {
          inSender: [
            "27PJTB",
            "31PJTB",
            "21PJTB",
            "23PJTB",
            "36PJTB",
            "41PJTB",
            "43PJTB",
            "56PJTB",
            "61PJTB",
            "63PJTB",
          ],
          // "outSender": [
          //     "03SERVER"
          // ],
          includeTags: ["BLASTETD"],
          excludeTags: [],
          messages: [
            {
              caption: `✈ *Promo Liburan EXTRA - BCA Holiday Travel Fair dari Panorama JTB Bandung!*🛍\n*Discount hingga Rp 1,5Juta* untuk tour ke destinasi impian Anda, bonus *Travel Voucher total Rp 5 Juta* untuk seluruh produk liburan di Panorama JTB dan *promo tiket pesawat* ke destinasi pilihan Anda mulai dari Korea, Jepang, Eropa hingga Amerika. \nKunjungi kemewahan *‘The City of Dreams’ Dubai, megahnya Anfield Stadium di *Great Britain*, jelajahi wahana permainan di *Tokyo Disneysea & Universal Studios Japan*, dan liburan mewah di *cruise mulai dari Rp 6 jutaan!*\nSegera booking liburan Anda dengan me-reply wa ini atau hubungi ⬇\nPanorama JTB Bandung\n📍 *Jl. Sunda no 76*\n☎  *022 - 420 - 8007*\n#BCAHolidayTravelFair`,
              image: {
                url: "https://id1.sgp1.digitaloceanspaces.com/timkado1/QHLNSGU3JY.jpg",
              },
            },
          ],
          label: "SQTF - PJTB",
          userId: "Admin",
        },
      },
    };
    // console.log(usr.data.datda.allUsers);
    // console.log(payload.Materi);

    const { data } = await processingMsgQueue({ payload: payloadBc, token });
    return data;
    return payloadBc;
  } catch (error) {
    console.log(error);
  }
};
