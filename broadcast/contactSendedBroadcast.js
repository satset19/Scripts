const { toJson, waitFor, toExcel, readExcel } = require("../helper/helper");
const { getTasks } = require("../services/BroadCastController");
const { checkMessages } = require("../services/MessageController");

const master = require("../master.json");
async function* asyncDataGenerator(token, payloads) {
  for (const payload of payloads) {
    // yield await fetchTasks(token, payload);
    yield await fetchChats(token, await fetchTasks(token, payload));
    await waitFor(1000);
  }
}

const fetchTasks = async (token, payloads) => {
  try {
    const { Sender, BatchId } = payloads;
    const filter = { batch: BatchId, sender: Sender };
    const { data } = await getTasks({ token, filter });
    return { tasks: data.data.tasks, task: data.data.tasks[0] };
  } catch (error) {
    console.log(error);
  }
};

const fetchChats = async (token, payload) => {
  //   console.log(payload.task);
  var date = new Date();

  // add a day

  try {
    const filter = {
      "msg.imageMessage.url": payload.task.msg[0].url,
      "msg.imageMessage.caption": payload.task.msg[0].caption,
      owner: payload.task.sender,
      //   createdAt: {
      //     gt: `${date.setDate(date.getDate() - 1)}`,
      //   },
    };
    const { data } = await checkMessages({ token, filter });
    // console.log(data);
    const notSend = payload.tasks
      .filter(
        (el) => data.data.msgs.map((ele) => ele.to).includes(el.phone) === false
      )
      .map((el) => {
        return { notSend: el.phone, sender: payload.task.sender };
      });

    const send = data.data.msgs.map((el) => {
      return { sended: el.to, sender: payload.task.sender };
    });
    const result = send.concat(notSend);

    // const blasted = master.map((ele) => {
    //   if (ele.TAGS !== "INTERNAL") {
    //     if (
    //       send.findIndex(
    //         (el) => ele.Phone == el.sended && ele.Owner === el.sender
    //       ) !== -1
    //     ) {
    //       ele["TAG 3"] = "BLASTED";
    //       return ele;
    //     }
    //     if (
    //       notSend.findIndex(
    //         (el) => ele.Phone == el.notSend && ele.Owner === el.sender
    //       ) !== -1
    //     ) {
    //       ele["TAG 3"] = "NOTYET";
    //       return ele;
    //     }
    //   }
    //   return ele;
    // });
    // return blasted;
    toJson(`${payload.task.sender}`, payload);
    // toExcel(`${payload.task.sender}`, result);
    console.log(payload.task.sender);
    return { send, notSend };
  } catch (error) {
    console.log(error);
  }
};

const token =
  "1241fb7c2a96a983aaa0fb6632ded68c8c2cb355ad474dcdda668c0ff862e7bd";
const payloads = [
  // { Sender: "18PJTB", BatchId: "07c002a6f7" },
  // { Sender: "08PJTB", BatchId: "629cf6e6f0" },
  { Sender: "10PJTB", BatchId: "22415ff766" },
  // { Sender: "13PJTB", BatchId: "8cba4b6b6b" },
  // { Sender: "15PJTB", BatchId: "c75c23b67e" },
  // { Sender: "20PJTB", BatchId: "1a166b007e" },
  // { Sender: "35PJTB", BatchId: "6fc58a69b7" },
  // { Sender: "36PJTB", BatchId: "a8ef2daea4" },
  // { Sender: "38PJTB", BatchId: "ac9748c3cd" },
  // { Sender: "39PJTB", BatchId: "9b7f26daca" },
  // { Sender: "40PJTB", BatchId: "789a6fda21" },
  // { Sender: "43PJTB", BatchId: "bf0c15f5b6" },
  // { Sender: "44PJTB", BatchId: "ba761699d7" },
  // { Sender: "47PJTB", BatchId: "430c8a7728" },
  // { Sender: "48PJTB", BatchId: "ee220d5244" },
  // { Sender: "49PJTB", BatchId: "61b409e207" },
  // { Sender: "52PJTB", BatchId: "a74e8c9ebd" },
  // { Sender: "53PJTB", BatchId: "ed13ab2321" },
  // { Sender: "54PJTB", BatchId: "47b76c78d5" },
  // { Sender: "55PJTB", BatchId: "353f78f773" },
  // { Sender: "56PJTB", BatchId: "4667c8447f" },
  // { Sender: "60PJTB", BatchId: "dfc8bc3fec" },
  // { Sender: "63PJTB", BatchId: "06a624e86a" },
  // { Sender: "64PJTB", BatchId: "64d1344ebc" },
  // { Sender: "65PJTB", BatchId: "841fff94cf" },
];

let send = [];
let notSend = [];
async function main() {
  for await (const res of asyncDataGenerator(token, payloads)) {
    send = send.concat(res.send);
    notSend = notSend.concat(res.notSend);

    // if(result.length === 0){
    //   result = res
    // }else{
    //   result = result.map
    // }
  }
  const blasted = master.map((ele) => {
    if (ele.TAGS !== "INTERNAL") {
      if (
        send.findIndex(
          (el) => ele.Phone == el.sended && ele.Owner === el.sender
        ) !== -1
      ) {
        ele["TAG 3"] = "BLASTED";
        return ele;
      }
      if (
        notSend.findIndex(
          (el) => ele.Phone == el.notSend && ele.Owner === el.sender
        ) !== -1
      ) {
        ele["TAG 3"] = "NOTYET";
        return ele;
      }
    }
    return ele;
  });
  // toJson("pp", send.concat(notSend));
  // toExcel("parno", blasted);
  // console.log(result)cle;
}
main();
