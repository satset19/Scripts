const { toJson, waitFor, toExcel } = require("../helper/helper");
const {
  broadcastProcess,
  getTasks,
} = require("../services/BroadCastController");

const payload = require("./checkBc.json");

const getProgress = async (broadcast) => {
  try {
    const payload = {
      sender: broadcast.sender,
      batchId: broadcast.batchId,
    };
    // console.log(payload);
    const { data } = await broadcastProcess(payload, token);
    // console.log(data);
    if (data.ok) {
      const res = {
        sender: broadcast.sender,
        total: data.result.total,
        terkirim: data.result.n,
        status: data.result.status,
      };
      /////////////////////////////
      // const res = {
      //   sender: payload.sender,
      //   status: `${data.result.n}/${data.result.total}`,
      //   s:
      //     data.result.n === data.result.total ||
      //     data.result.n >= data.result.total ||
      //     data.result.status === "DONE"
      //       ? "DONE"
      //       : "IN_PROGRESS",
      // };
      // console.log(
      //   `${res.sender}: ${res.status}   ${res.s}  ${payload.batchId}`
      // );
      // if (res.s === "IN_PROGRESS") {
      //   console.log(
      //     `${res.sender}: ${res.status}   ${res.s}  ${payload.batchId}`
      //   );
      // }
      return res;
    }

    // const { data } = await broadcastProcess(payload, token);
    // // console.log(data);
    // if (data.ok) {
    //   const res = {
    //     sender: payload.sender,
    //     status: `${data.result.n}/${data.result.total}`,
    //     s:
    //       data.result.n === data.result.total ||
    //       data.result.n >= data.result.total ||
    //       data.result.status === "DONE"
    //         ? "DONE"
    //         : "IN_PROGRESS",
    //   };
    //   // console.log(
    //   //   `${res.sender}: ${res.status}   ${res.s}  ${payload.batchId}`
    //   // );
    //   if (res.s === "IN_PROGRESS") {
    //     console.log(
    //       `${res.sender}: ${res.status}   ${res.s}  ${payload.batchId}`
    //     );
    //   }
    // }
  } catch (error) {
    console.log(error);
  }
};

const reprocess = async (sender, batchId, token) => {
  try {
  } catch (error) {}
};

const fotchTask = async (token) => {
  try {
    const { data } = await getTasks({ token, filter: { batch: "4397b85fd2" } });
    console.log(data.data.tasks);
  } catch (error) {
    console.log(error);
  }
};

const token =
  "dcdf26cef7248a1f200140e8329f8a242ce777f0e54246089b913d199605aad5b7";

// const payload = [
//   { sender: "01DWIDAY", batchId: "b194c9b69f" },
//   { sender: "01DWIDAY", batchId: "eb2128380d" },
//   { sender: "06DWIDAY", batchId: "10a45f4738" },
//   { sender: "06DWIDAY", batchId: "a7fb3a802a" },
//   { sender: "07DWIDAY", batchId: "5cda514886" },
//   { sender: "07DWIDAY", batchId: "bebb2ae6b9" },
//   { sender: "09DWIDAY", batchId: "64e6f91cdf" },
//   { sender: "09DWIDAY", batchId: "87ca86c244" },
//   { sender: "12DWIDAY", batchId: "1e8a319088" },
//   { sender: "14DWIDAY", batchId: "acc832ee72" },
//   { sender: "16DWIDAY", batchId: "ee28eed299" },
//   { sender: "17DWIDAY", batchId: "2193c57323" },
//   { sender: "19DWIDAY", batchId: "77b6b0bde7" },
//   { sender: "19DWIDAY", batchId: "edab82689f" },
//   { sender: "21DWIDAY", batchId: "e01929aa5d" },
//   { sender: "22DWIDAY", batchId: "95da65a8e7" },
//   { sender: "35DWIDAY", batchId: "b9c3d6ae1b" },
//   { sender: "36DWIDAY", batchId: "8e885903c4" },
//   { sender: "37DWIDAY", batchId: "809a101531" },
//   { sender: "39DWIDAY", batchId: "198a400835" },
//   { sender: "45DWIDAY", batchId: "921627c5c5" },
//   { sender: "46DWIDAY", batchId: "8573a3cbdb" },
//   { sender: "46DWIDAY", batchId: "8fde9eacce" },
//   { sender: "47DWIDAY", batchId: "94bf89a0da" },
//   { sender: "48DWIDAY", batchId: "575b0862a8" },
// ];

// payload.forEach(async (el) => {
//   await getProgress(el, token);
// });

// fotchTask(token);
// waitFor(3000);
// paload2.forEach(async (el) => {
//   await getProgress(el, token);
// });
// waitFor(3000);
// paload3.forEach(async (el) => {
//   await getProgress(el, token);
// });
// getProgress({ sender: "04DWIDAY", batchId: "733b2fb54a" }, token);

// console.log(payload);

async function* asyncDataGenerator() {
  for (const broadcast of payload) {
    yield await getProgress(broadcast);
  }
}

async function main() {
  const finalData = [];
  for await (const data of asyncDataGenerator()) {
    finalData.push(data);
    // console.log(data);
  }
  toExcel("PJTBBroadcast", finalData);
  // console.log(finalData);
}

main();
// console.log(payload);
