const { waitFor, readExcel } = require("../helper/helper.js");
const { processingMsgQueue } = require("../services/BroadCastController.js");
const { getAllUsers } = require("../services/UserController.js");
const path = "./PanoramaBroadcastPlan.xlsx";
const token =
  "d405a1c9c00571708a7ab8d877f3163b72f2f563c529d7d2b99361cc9e0bfc6f";

const createBroadcast = async (sender) => {
  try {
    const usr = await getAllUsers({ filter: { "payload.sender": sender } });
    const token = usr.data.data.allUsers.allUsers[0].payload.token;
    const user = usr.data.data.allUsers.allUsers[0];
    // console.log(user.name);
    const time = "22/02/2024 12:00".split("/");
    // time.pop();
    const payloadBc = {
      data: {
        body: {
          label: "SQTF - BCA",
          messages: [
            {
              caption: `📣 *Booking tiket Singapore Airlines di Panorama JTB!* ✈🤩
              Pilih bonusnya: *MacBook Air atau Apple Watch!*
              Ada juga *Cashback Hingga Rp 2 Juta* dari Bank BCA, *Travel Voucher Hingga Rp 5 Juta*, *hingga Paket Tour ke Korea!* Dan dapatkan *2X Panorama Points* 
              Hubungi Panorama JTB sekarang juga! 🤩`,
              image: {
                url: "https://id1.sgp1.digitaloceanspaces.com/timkado1/dIjUe94Sf8.jpg",
              },
            },
          ],
          tags: "CUSTOMER_BLAST",
          //   excludes: "",
          sender,
          userId: user.name,
        },
        headers: {
          "Content-Type": "application/json",
          token,
        },
      },
      interval: `${time[1].slice(1)}/${time[0]}/${time[2]}`,
      name: "send-wa-v2",
      type: "schedule",
    };
    // console.log(usr.data.datda.allUsers);
    // console.log(payload.Materi);

    // const { data } = await processingMsgQueue({ payload: payloadBc, token });
    // return data;
    return payloadBc;
  } catch (error) {
    console.log(error);
  }
};

async function* asyncDataGenerator(senders, token) {
  for (const payload of senders) {
    yield await createBroadcast(payload, token);
    await waitFor(1000);
  }
}

const senders = [
  "08PJTB",
  "09PJTB",
  "13PJTB",
  "14PJTB",
  "15PJTB",
  "17PJTB",
  "86PJTB",
  "20PJTB",
  "21PJTB",
  "22PJTB",
  "23PJTB",
  "24PJTB",
  "25PJTB",
  "35PJTB",
  "36PJTB",
  "37PJTB",
  "39PJTB",
  "41PJTB",
  "45PJTB",
  "46PJTB",
  "47PJTB",
  "48PJTB",
  "56PJTB",
  "57PJTB",
  "60PJTB",
  "74PJTB",
  "65PJTB",
  "66PJTB",
];
// console.log(senders.length);

async function main() {
  try {
    let i = 0;
    for await (const data of asyncDataGenerator(senders, token)) {
      i++;
      console.log(data);
      console.log(i);
    }
  } catch (error) {
    console.log(error);
  }
}

main();
